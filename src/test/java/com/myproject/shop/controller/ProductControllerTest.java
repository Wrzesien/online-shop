package com.myproject.shop.controller;

import com.myproject.shop.infrastructure.repository.CommentHibernateRepository;
import com.myproject.shop.infrastructure.repository.ProductHibernateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CommentHibernateRepository commentHibernateRepository;

    @Autowired
    private ProductHibernateRepository productHibernateRepository;

    @Test
    public void ifCreateProductRequestContainsCorrectData_ShouldReturnHttpCode200AndCreateProduct() throws Exception {
        commentHibernateRepository.deleteAll();
        productHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/product/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Samsung 32HD\",\n" +
                                        "\t\"category\": \"TV\",\n" +
                                        "\t\"price\": 800,\n" +
                                        "\t\"amount\": 8\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

    @Test
    public void ifCreateProductRequestContainsPriceLessThan0_ShouldReturnHttpCode400andErrorMessage() throws Exception {
        mockMvc
                .perform(post("/product/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Samsung 32HD\",\n" +
                                        "\t\"category\": \"TV\",\n" +
                                        "\t\"price\": -1800,\n" +
                                        "\t\"amount\": 8\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR008\",\n" +
                                "\t\"errorMsg\": \"Cena nie może być mniejsza od 0!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateProductRequestContainsAmountLessThan0_ShouldReturnHttpCode400andErrorMessage() throws Exception {
        mockMvc
                .perform(post("/product/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Samsung 32HD\",\n" +
                                        "\t\"category\": \"TV\",\n" +
                                        "\t\"price\": 800,\n" +
                                        "\t\"amount\": -4\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR009\",\n" +
                                "\t\"errorMsg\": \"Ilość nie może być mniejsza od 0!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetProductRequestIsCorrect_ShouldReturnHttpCode200AndProduct() throws Exception {
        mockMvc
                .perform(get("/product/get/{id}", 1))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"name\": \"Samsung S20\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 3900,\n" +
                                "\t\"amount\": 2\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetProductRequestContainsNotExistProductId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/product/get/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR004\",\n" +
                                "\t\"errorMsg\": \"Nie ma produktu o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetAllProductsRequestIsCorrect_ShouldReturnHttpCode200AndProductList() throws Exception {
        mockMvc
                .perform(get("/product/getAll"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"name\": \"Samsung S20\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 3900,\n" +
                                "\t\"amount\": 2\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 2,\n" +
                                "\t\"name\": \"Samsung S20+\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 4400,\n" +
                                "\t\"amount\": 10\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 3,\n" +
                                "\t\"name\": \"Samsung S10\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 3200,\n" +
                                "\t\"amount\": 4\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 4,\n" +
                                "\t\"name\": \"Samsung S10+\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 3800,\n" +
                                "\t\"amount\": 10\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 5,\n" +
                                "\t\"name\": \"Apple iPhone SE 2020 64GB\",\n" +
                                "\t\"category\": \"TELEFONY\",\n" +
                                "\t\"price\": 2200,\n" +
                                "\t\"amount\": 5\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 6,\n" +
                                "\t\"name\": \"Sony LED KD-55XG7077\",\n" +
                                "\t\"category\": \"TV\",\n" +
                                "\t\"price\": 2200,\n" +
                                "\t\"amount\": 10\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 7,\n" +
                                "\t\"name\": \"Samsung QLED QE55Q64R\",\n" +
                                "\t\"category\": \"TV\",\n" +
                                "\t\"price\": 3500,\n" +
                                "\t\"amount\": 12\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 8,\n" +
                                "\t\"name\": \"Panasonic LED TX-58GX810E\",\n" +
                                "\t\"category\": \"TV\",\n" +
                                "\t\"price\": 2800,\n" +
                                "\t\"amount\": 5\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 9,\n" +
                                "\t\"name\": \"Sharp LED 50BL5EA\",\n" +
                                "\t\"category\": \"TV\",\n" +
                                "\t\"price\": 1500,\n" +
                                "\t\"amount\": 18\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 10,\n" +
                                "\t\"name\": \"UGO Mistral 2.0\",\n" +
                                "\t\"category\": \"DRONY\",\n" +
                                "\t\"price\": 230,\n" +
                                "\t\"amount\": 25\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 11,\n" +
                                "\t\"name\": \"Overmax X-bee drone 3.3 Wi-Fi\",\n" +
                                "\t\"category\": \"DRONY\",\n" +
                                "\t\"price\": 250,\n" +
                                "\t\"amount\": 14\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\": 12,\n" +
                                "\t\"name\": \"Blaupunkt ESC808\",\n" +
                                "\t\"category\": \"HULAJNOGI\",\n" +
                                "\t\"price\": 1600,\n" +
                                "\t\"amount\": 2\n" +
                                "}]"
                ));
    }

    @Test
    public void ifDeleteRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        commentHibernateRepository.deleteAll();
        mockMvc
                .perform(delete("/product/delete/{id}", 1))
                .andExpect(status().is(200));
    }

    @Test
    public void ifGetProductsInCategoryRequestIsCorrect_ShouldReturnHttpCode200AndProductList() throws Exception {
        mockMvc
                .perform(get("/product/getProductsInCategory").param("category", "DRONY"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\":10,\n" +
                                "\t\"name\":\"UGO Mistral 2.0\",\n" +
                                "\t\"category\":\"DRONY\",\n" +
                                "\t\"price\":230.0,\n" +
                                "\t\"amount\":25\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":11,\n" +
                                "\t\"name\":\"Overmax X-bee drone 3.3 Wi-Fi\",\n" +
                                "\t\"category\":\"DRONY\",\n" +
                                "\t\"price\":250.0,\n" +
                                "\t\"amount\":14\n" +
                                "}]"
                ));
    }

    @Test
    public void ifGetProductsByFragmentOfNameIsCorrect_ShouldReturnHttpCode200AndProductList() throws Exception {
        mockMvc
                .perform(get("/product/getProductsByFragmentOfName").param("nameFragment", "S20"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\":1,\n" +
                                "\t\"name\":\"Samsung S20\",\n" +
                                "\t\"category\":\"TELEFONY\",\n" +
                                "\t\"price\":3900.0,\n" +
                                "\t\"amount\":2\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":2,\n" +
                                "\t\"name\":\"Samsung S20+\",\n" +
                                "\t\"category\":\"TELEFONY\",\n" +
                                "\t\"price\":4400.0,\n" +
                                "\t\"amount\":10\n" +
                                "}]"
                ));
    }
}