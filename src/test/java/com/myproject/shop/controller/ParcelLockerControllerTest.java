package com.myproject.shop.controller;

import com.myproject.shop.infrastructure.repository.ParcelLockerHibernateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ParcelLockerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ParcelLockerHibernateRepository parcelLockerHibernateRepository;

    @Test
    public void ifCreateParcelLockerRequestContainsCorrectData_ShouldReturnHttpCode200AndCreateParcelLocker() throws Exception {
        parcelLockerHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/parcel_locker/add")
                        .content(
                                "{\n" +
                                        "\t\"address\": \"Galeria Echo - Kielce\",\n" +
                                        "\t\"longitude\": 50.52,\n" +
                                        "\t\"latitude\": 20.38\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

    @Test
    public void ifCreateParcelLockerRequestContainsUncorrectedLatitude_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/parcel_locker/add")
                        .content(
                                "{\n" +
                                        "\t\"address\": \"Galeria Echo - Kielce\",\n" +
                                        "\t\"longitude\": 220.52,\n" +
                                        "\t\"latitude\": 20.38\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR011\",\n" +
                                "\t\"errorMsg\": \"Długość geograficzna powinna mieścić się w przedziale od -180 do 180!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateParcelLockerRequestContainsUncorrectedLongitude_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/parcel_locker/add")
                        .content(
                                "{\n" +
                                        "\t\"address\": \"Galeria Echo - Kielce\",\n" +
                                        "\t\"longitude\": 50.52,\n" +
                                        "\t\"latitude\": 120.38\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR012\",\n" +
                                "\t\"errorMsg\": \"Szerokość geograficzna powinna mieścić się w przedziale od -90 do 90!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetParcelLockerRequestIsCorrect_ShouldReturnHttpCode200AndParcelLocker() throws Exception {
        mockMvc
                .perform(get("/parcel_locker/get/{id}", 1))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"address\": \"CH Arkadia\",\n" +
                                "\t\"longitude\": 52.15,\n" +
                                "\t\"latitude\": 20.59\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetParcelLockerRequestContainsNotExistParcelLockerId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/parcel_locker/get/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR001\",\n" +
                                "\t\"errorMsg\": \"Nie ma paczkomatu o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetAllParcelLockersRequestIsCorrect_ShouldReturnHttpCode200AndParcelLockerList() throws Exception {
        mockMvc
                .perform(get("/parcel_locker/getAll"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"address\": \"CH Arkadia\",\n" +
                                "\t\"longitude\": 52.15,\n" +
                                "\t\"latitude\": 20.59\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":2,\n" +
                                "\t\"address\":\"Galeria Północna\",\n" +
                                "\t\"longitude\":52.31,\n" +
                                "\t\"latitude\":20.96\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":3,\n" +
                                "\t\"address\":\"Atrium Reduta\",\n" +
                                "\t\"longitude\":52.12,\n" +
                                "\t\"latitude\":20.57\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":4,\n" +
                                "\t\"address\":\"Galeria Młociny\",\n" +
                                "\t\"longitude\":52.17,\n" +
                                "\t\"latitude\":20.55\n" +
                                "}]"
                ));
    }

    @Test
    public void ifDeleteRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(delete("/parcel_locker/delete/{id}", 1))
                .andExpect(status().is(200));
    }
}
