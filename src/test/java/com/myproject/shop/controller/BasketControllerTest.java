package com.myproject.shop.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BasketControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void ifCreateBasketRequestContainsCorrectData_ShouldReturnHttpCode200AndCreateBasket() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().is(200));
    }

    @Test
    public void ifCreateBasketRequestContainsNotExistClientId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 15,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR002\",\n" +
                                "\t\"errorMsg\": \"Nie ma Klienta o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateBasketRequestContainsNotExistProductId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,15]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR004\",\n" +
                                "\t\"errorMsg\": \"Nie ma produktu o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifAddToBasketRequestContainsCorrectData_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(put("/basket/addToBasket")
                        .content(
                                "{\n" +
                                        "\t\"basketId\": 1,\n" +
                                        "\t\"productIdsList\": [4,5,8]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

    @Test
    public void ifAddToBasketRequestContainsNotExistBasketId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(put("/basket/addToBasket")
                        .content(
                                "{\n" +
                                        "\t\"basketId\": 15,\n" +
                                        "\t\"productIdsList\": [4,5,8]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR005\",\n" +
                                "\t\"errorMsg\": \"Nie ma koszyka o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifAddToBasketRequestContainsNotExistProductId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(put("/basket/addToBasket")
                        .content(
                                "{\n" +
                                        "\t\"basketId\": 1,\n" +
                                        "\t\"productIdsList\": [4,5,15]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR004\",\n" +
                                "\t\"errorMsg\": \"Nie ma produktu o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetBasketRequestIsCorrect_ShouldReturnHttpCode200AndBasket() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(get("/basket/get/{id}", 1))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.clientId").value(1))
                .andExpect(jsonPath("$.parcelLockerId").value(1))
                .andExpect(jsonPath("$.totalAmount").value(7885.0))
                .andExpect(jsonPath("$.status").value("STARTED"))
                .andExpect(jsonPath("$.products[0]").value(1))
                .andExpect(jsonPath("$.products[1]").value(2));
    }

    @Test
    public void ifGetBasketRequestContainsNotExistBasketId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/basket/get/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR005\",\n" +
                                "\t\"errorMsg\": \"Nie ma koszyka o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetAllBasketsRequestIsCorrect_ShouldReturnHttpCode200AndBasketList() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 5,\n" +
                                        "\t\"productIdsList\": [8]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(get("/basket/getAll"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].clientId").value(1))
                .andExpect(jsonPath("$[0].parcelLockerId").value(1))
                .andExpect(jsonPath("$[0].totalAmount").value(7885.0))
                .andExpect(jsonPath("$[0].status").value("STARTED"))
                .andExpect(jsonPath("$[0].products[0]").value(1))
                .andExpect(jsonPath("$[0].products[1]").value(2))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].clientId").value(5))
                .andExpect(jsonPath("$[1].parcelLockerId").value(1))
                .andExpect(jsonPath("$[1].totalAmount").value(2744.0))
                .andExpect(jsonPath("$[1].status").value("STARTED"))
                .andExpect(jsonPath("$[1].products[0]").value(8));
    }

    @Test
    public void ifDeleteRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(delete("/basket/delete/{id}", 1))
                .andExpect(status().is(200));
    }

    @Test
    public void ifPayRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [10]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(put("/basket/pay/{id}", 1).param("parcelLockerId", "3"))
                .andExpect(status().is(200));
    }

    @Test
    public void ifPayRequestContainsTotalAmountHigherThanClientAccountBalance_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/basket/add")
                        .content(
                                "{\n" +
                                        "\t\"clientId\": 1,\n" +
                                        "\t\"productIdsList\": [1,2]\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(put("/basket/pay/{id}", 1).param("parcelLockerId", "3"))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR006\",\n" +
                                "\t\"errorMsg\": \"Nie masz wystarczającej kwoty na zapłatę za zakupy. Zasil rachunek !\"\n" +
                                "}"
                ));
    }
}
