package com.myproject.shop.controller;

import com.myproject.shop.infrastructure.repository.CommentHibernateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CommentHibernateRepository commentHibernateRepository;

    @Test
    public void ifCreateCommentRequestContainsCorrectData_ShouldReturnHttpCode200AndCreateComment() throws Exception {
        commentHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/comment/add")
                        .content(
                                "{\n" +
                                        "\t\"productId\":2,\n" +
                                        "\t\"clientId\":2,\n" +
                                        "\t\"content\":\"Doskonały na prezent\"\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

    @Test
    public void ifCreateCommentRequestContainsNotExistProductId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/comment/add")
                        .content(
                                "{\n" +
                                        "\t\"productId\":222,\n" +
                                        "\t\"clientId\":2,\n" +
                                        "\t\"content\":\"Doskonały na prezent\"\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR004\",\n" +
                                "\t\"errorMsg\": \"Nie ma produktu o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateCommentRequestContainsNotExistClientId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(post("/comment/add")
                        .content(
                                "{\n" +
                                        "\t\"productId\":2,\n" +
                                        "\t\"clientId\":222,\n" +
                                        "\t\"content\":\"Doskonały na prezent\"\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR002\",\n" +
                                "\t\"errorMsg\": \"Nie ma Klienta o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetCommentRequestIsCorrect_ShouldReturnHttpCode200AndComment() throws Exception {
        mockMvc
                .perform(get("/comment/get/{id}", 1))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"productId\": 1,\n" +
                                "\t\"clientId\": 1,\n" +
                                "\t\"content\": \"Super komórka\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetCommentRequestContainsNotExistCommentId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/comment/get/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR003\",\n" +
                                "\t\"errorMsg\": \"Nie ma komentarza o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetAllCommentsRequestIsCorrect_ShouldReturnHttpCode200AndCommentList() throws Exception {
        mockMvc
                .perform(get("/comment/getAll"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\":1,\n" +
                                "\t\"productId\":1,\n" +
                                "\t\"clientId\":1,\n" +
                                "\t\"content\":\"Super komórka\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":2,\n" +
                                "\t\"productId\":2,\n" +
                                "\t\"clientId\":3,\n" +
                                "\t\"content\":\"Spodziewałem się lepszej jakości\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":3,\n" +
                                "\t\"productId\":2,\n" +
                                "\t\"clientId\":1,\n" +
                                "\t\"content\":\"Trochę za duży wyświetlacz\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":4,\n" +
                                "\t\"productId\":1,\n" +
                                "\t\"clientId\":3,\n" +
                                "\t\"content\":\"Nieadekwatna cena do parametrów. Wolę Apple\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":5,\n" +
                                "\t\"productId\":8,\n" +
                                "\t\"clientId\":2,\n" +
                                "\t\"content\":\"Fajny sklep. Wszystko zgodnie z ustaleniami z mail-a od sklepu\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":6,\n" +
                                "\t\"productId\":5,\n" +
                                "\t\"clientId\":5,\n" +
                                "\t\"content\":\"Przesyłka przyszła z dwudniowym opóźnieniem. Poza tym OK\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":7,\n" +
                                "\t\"productId\":8,\n" +
                                "\t\"clientId\":1,\n" +
                                "\t\"content\":\"Telewizor miał porysowany ekran. Sklep zgodził się wymienić na nowy\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":8,\n" +
                                "\t\"productId\":4,\n" +
                                "\t\"clientId\":2,\n" +
                                "\t\"content\":\"Transakcja przebiegła prawidłowo\"\n" +
                                "}]"
                ));
    }

    @Test
    public void ifDeleteRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(delete("/comment/delete/{id}", 1))
                .andExpect(status().is(200));
    }

    @Test
    public void ifGetCommentsByClientIdRequestIsCorrect_ShouldReturnHttpCode200AndCommentList() throws Exception {
        mockMvc
                .perform(get("/comment/getByClientId/{id}", 2))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 5,\n" +
                                "\t\"productId\":8,\n" +
                                "\t\"clientId\":2,\n" +
                                "\t\"content\":\"Fajny sklep. Wszystko zgodnie z ustaleniami z mail-a od sklepu\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":8,\n" +
                                "\t\"productId\":4,\n" +
                                "\t\"clientId\":2,\n" +
                                "\t\"content\":\"Transakcja przebiegła prawidłowo\"\n" +
                                "}]"
                ));
    }

    @Test
    public void ifGetCommentsByClientIdRequestContainsNotExistClientId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/comment/getByClientId/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR002\",\n" +
                                "\t\"errorMsg\": \"Nie ma Klienta o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetCommentsByProductIdRequestIsCorrect_ShouldReturnHttpCode200AndCommentList() throws Exception {
        mockMvc
                .perform(get("/comment/getByProductId/{id}", 2))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 2,\n" +
                                "\t\"productId\":2,\n" +
                                "\t\"clientId\":3,\n" +
                                "\t\"content\":\"Spodziewałem się lepszej jakości\"\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":3,\n" +
                                "\t\"productId\":2,\n" +
                                "\t\"clientId\":1,\n" +
                                "\t\"content\":\"Trochę za duży wyświetlacz\"\n" +
                                "}]"
                ));
    }

    @Test
    public void ifGetCommentsByProductIdRequestContainsNotExistProductId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/comment/getByProductId/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR004\",\n" +
                                "\t\"errorMsg\": \"Nie ma produktu o takim numerze id!\"\n" +
                                "}"
                ));
    }
}
