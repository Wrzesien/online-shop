package com.myproject.shop.controller;

import com.myproject.shop.infrastructure.repository.ClientHibernateRepository;
import com.myproject.shop.infrastructure.repository.CommentHibernateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ClientControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ClientHibernateRepository clientHibernateRepository;

    @Autowired
    CommentHibernateRepository commentHibernateRepository;

    @Test
    public void ifCreateClientRequestContainsCorrectData_ShouldReturnHttpCode200AndCreateClient() throws Exception {
        commentHibernateRepository.deleteAll();
        clientHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28@wp.pl\",\n" +
                                        "\t\"discount\": 0.08,\n" +
                                        "\t\"accountBalance\": 3000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

    @Test
    public void ifCreateClientRequestContainsUncorrectedEmail_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        commentHibernateRepository.deleteAll();
        clientHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28wp.pl\",\n" +
                                        "\t\"discount\": 0.08,\n" +
                                        "\t\"accountBalance\": 3000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR015\",\n" +
                                "\t\"errorMsg\": \"Adres e-mail jest w niewłaściwym formacie!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateClientRequestContainsUncorrectedDiscount_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        commentHibernateRepository.deleteAll();
        clientHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28@wp.pl\",\n" +
                                        "\t\"discount\": -0.08,\n" +
                                        "\t\"accountBalance\": 3000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR016\",\n" +
                                "\t\"errorMsg\": \"Zniżka powinna mieścić się w przedziale od 0,00 do 1,00!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateClientRequestContainsUncorrectedAccountBalance_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        commentHibernateRepository.deleteAll();
        clientHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28@wp.pl\",\n" +
                                        "\t\"discount\": 0.08,\n" +
                                        "\t\"accountBalance\": -5000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR017\",\n" +
                                "\t\"errorMsg\": \"Saldo na rachunku nie może być ujemne!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifCreateClientRequestContainsEmailAlreadyExistInRepository_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        commentHibernateRepository.deleteAll();
        clientHibernateRepository.deleteAll();
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28@wp.pl\",\n" +
                                        "\t\"discount\": 0.08,\n" +
                                        "\t\"accountBalance\": 3000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON));
        mockMvc
                .perform(post("/client/add")
                        .content(
                                "{\n" +
                                        "\t\"name\": \"Artur\",\n" +
                                        "\t\"lastName\": \"Zawisza\",\n" +
                                        "\t\"email\": \"a.zawisza28@wp.pl\",\n" +
                                        "\t\"discount\": 0.08,\n" +
                                        "\t\"accountBalance\": 3000.0\n" +
                                        "}"
                        )
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR030\",\n" +
                                "\t\"errorMsg\": \"Taki adres email istnieje już w bazie!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetClientRequestIsCorrect_ShouldReturnHttpCode200AndClient() throws Exception {
        mockMvc
                .perform(get("/client/get/{id}", 1))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"name\": \"Anna\",\n" +
                                "\t\"lastName\": \"Kowalska\",\n" +
                                "\t\"email\": \"a.kowalska@gmail.com\",\n" +
                                "\t\"discount\": 0.05,\n" +
                                "\t\"accountBalance\": 5000.0\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetClientRequestContainsNotExistClientId_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(get("/client/get/{id}", 15))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR002\",\n" +
                                "\t\"errorMsg\": \"Nie ma Klienta o takim numerze id!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetAllClientsRequestIsCorrect_ShouldReturnHttpCode200AndClientList() throws Exception {
        mockMvc
                .perform(get("/client/getAll"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 1,\n" +
                                "\t\"name\": \"Anna\",\n" +
                                "\t\"lastName\": \"Kowalska\",\n" +
                                "\t\"email\": \"a.kowalska@gmail.com\",\n" +
                                "\t\"discount\": 0.05,\n" +
                                "\t\"accountBalance\": 5000.0\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":2,\n" +
                                "\t\"name\":\"Tomasz\",\n" +
                                "\t\"lastName\":\"Maruda\",\n" +
                                "\t\"email\":\"maruda@gmail.com\",\n" +
                                "\t\"discount\":0.02,\n" +
                                "\t\"accountBalance\":3000.0\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":3,\n" +
                                "\t\"name\":\"Edyta\",\n" +
                                "\t\"lastName\":\"Polak\",\n" +
                                "\t\"email\":\"polakedyta@wp.pl\",\n" +
                                "\t\"discount\":0.1,\n" +
                                "\t\"accountBalance\":4500.0\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":4,\n" +
                                "\t\"name\":\"Maciej\",\n" +
                                "\t\"lastName\":\"Nowy\",\n" +
                                "\t\"email\":\"m.nowy@poczta.onet.pl\",\n" +
                                "\t\"discount\":0.05,\n" +
                                "\t\"accountBalance\":3700.0\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":5,\n" +
                                "\t\"name\":\"Karolina\",\n" +
                                "\t\"lastName\":\"Zawadzka\",\n" +
                                "\t\"email\":\"zawadka18@wp.pl\",\n" +
                                "\t\"discount\":0.02,\n" +
                                "\t\"accountBalance\":1200.0\n" +
                                "}]"
                ));
    }

    @Test
    public void ifDeleteRequestIsCorrect_ShouldReturnHttpCode200() throws Exception {
        commentHibernateRepository.deleteAll();
        mockMvc
                .perform(delete("/client/delete/{id}", 1))
                .andExpect(status().is(200));
    }

    @Test
    public void ifTransferToClientAccountRequestContainsCorrectData_ShouldReturnHttpCode200() throws Exception {
        mockMvc
                .perform(put("/client/transferAmount/{id}", 1).param("transferAmount", "500"))
                .andExpect(status().is(200));
    }

    @Test
    public void ifTransferToClientAccountAmountLessThanOrEqual0_ShouldReturnHttpCode400AndErrorMessage() throws Exception {
        mockMvc
                .perform(put("/client/transferAmount/{id}", 1).param("transferAmount", "-800"))
                .andExpect(status().is(400))
                .andExpect(content().json(
                        "{\n" +
                                "\t\"errorCode\": \"ERR019\",\n" +
                                "\t\"errorMsg\": \"Kwota jaką chcesz zasilić rachunek nie może być mniejsza lub równa 0!\"\n" +
                                "}"
                ));
    }

    @Test
    public void ifGetClientsByFragmentOfEmailRequestContainsCorrectData_ShouldReturnHttpCode200AndClientList() throws Exception {
        mockMvc
                .perform(get("/client/getClientsByFragmentOfEmail").param("emailFragment", "wp"))
                .andExpect(status().is(200))
                .andExpect(content().json(
                        "[{\n" +
                                "\t\"id\": 3,\n" +
                                "\t\"name\":\"Edyta\",\n" +
                                "\t\"lastName\":\"Polak\",\n" +
                                "\t\"email\":\"polakedyta@wp.pl\",\n" +
                                "\t\"discount\":0.1,\n" +
                                "\t\"accountBalance\":4500.0\n" +
                                "},\n" +
                                "{\n" +
                                "\t\"id\":5,\n" +
                                "\t\"name\":\"Karolina\",\n" +
                                "\t\"lastName\":\"Zawadzka\",\n" +
                                "\t\"email\":\"zawadka18@wp.pl\",\n" +
                                "\t\"discount\":0.02,\n" +
                                "\t\"accountBalance\":1200.0\n" +
                                "}]"
                ));
    }
}
