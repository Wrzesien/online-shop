package com.myproject.shop.domain.model;

import java.util.ArrayList;
import java.util.List;

public class ParcelLocker {

    private Long id;
    private String address;
    private Double longitude;
    private Double latitude;
    List<Long> basketIds;

    public ParcelLocker(String address, Double longitude, Double latitude) {
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public ParcelLocker(Long id, String address, Double longitude, Double latitude) {
        this.id = id;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<Long> getBaskets() {
        if(basketIds == null){
            return basketIds = new ArrayList<>();
        }
        return basketIds;
    }

    public void setBaskets(List<Long> basketIds) {
        this.basketIds = basketIds;
    }
}
