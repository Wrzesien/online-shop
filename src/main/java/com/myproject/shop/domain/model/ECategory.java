package com.myproject.shop.domain.model;

public enum ECategory {

    TELEFONY, TV, PRALKI, LODÓWKI, HULAJNOGI, DRONY
}
