package com.myproject.shop.domain.model;

public class Comment {

    private Long id;
    private Long productId;
    private Long clientId;
    private String content;

    public Comment(Long productId, Long clientId, String content) {
        this.productId = productId;
        this.clientId = clientId;
        this.content = content;
    }

    public Comment(Long id, Long productId, Long clientId, String content) {
        this.id = id;
        this.productId = productId;
        this.clientId = clientId;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}