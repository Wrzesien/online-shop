package com.myproject.shop.domain.model;

import java.time.LocalDateTime;
import java.util.List;

public class Basket {

    private Long id;
    private Long clientId;
    private Long parcelLockerId;
    private LocalDateTime date;
    private Double totalAmount;
    private EStatus status;
    private List<Long> products;

    public Basket(Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<Long> products) {
        this.clientId = clientId;
        this.parcelLockerId = parcelLockerId;
        this.date = date;
        this.totalAmount = totalAmount;
        this.status = status;
        this.products = products;
    }

    public Basket(Long id, Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<Long> products) {
        this.id = id;
        this.clientId = clientId;
        this.parcelLockerId = parcelLockerId;
        this.date = date;
        this.totalAmount = totalAmount;
        this.status = status;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getParcelLockerId() {
        return parcelLockerId;
    }

    public void setParcelLockerId(Long parcelLockerId) {
        this.parcelLockerId = parcelLockerId;
    }

    public List<Long> getProducts() {
        return products;
    }

    public void setProducts(List<Long> products) {
        this.products = products;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(EStatus status) {
        this.status = status;
    }
}
