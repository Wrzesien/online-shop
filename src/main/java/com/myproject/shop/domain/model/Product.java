package com.myproject.shop.domain.model;

public class Product {

    private Long id;
    private String name;
    private ECategory category;
    private Double price;
    private Integer amount;

    public Product(String name, ECategory category, Double price, Integer amount) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.amount = amount;
    }

    public Product(Long id, String name, ECategory category, Double price, Integer amount) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ECategory getCategory() {
        return category;
    }

    public void setCategory(ECategory category) {
        this.category = category;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}


