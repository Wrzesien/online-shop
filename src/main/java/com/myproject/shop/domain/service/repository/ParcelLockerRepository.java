package com.myproject.shop.domain.service.repository;

import com.myproject.shop.domain.model.ParcelLocker;
import com.myproject.shop.infrastructure.entity.ParcelLockerHibernate;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ParcelLockerRepository {

    ParcelLocker createParcelLocker(String address,
                                    Double longitude,
                                    Double latitude);

    Optional<ParcelLocker> getParcelLocker(Long id);

    List<ParcelLocker> getAllParcelLockers(Pageable pageable);

    void removeParcelLocker(Long id);

    ParcelLocker updateParcelLocker(Long id,
                                    String address,
                                    Double longitude,
                                    Double latitude);

    ParcelLocker toDomain(ParcelLockerHibernate parcelLockerHibernate);
}
