package com.myproject.shop.domain.service.impl;

import com.myproject.shop.common.AbstractCommonService;
import com.myproject.shop.common.MsgSource;
import com.myproject.shop.common.ValidationUtils;
import com.myproject.shop.domain.model.Client;
import com.myproject.shop.domain.service.repository.ClientRepository;
import com.myproject.shop.domain.service.service.ClientService;
import com.myproject.shop.exception.CommonBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl extends AbstractCommonService implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(MsgSource msgSource, ClientRepository clientRepository) {
        super(msgSource);
        this.clientRepository = clientRepository;
    }

    @Override
    public Long createClient(Client client) {

        validateCreateClientRequest(client);

        Client repositoryClient = clientRepository.createClient(
                client.getName(),
                client.getLastName(),
                client.getEmail(),
                client.getDiscount(),
                client.getAccountBalance());

        return repositoryClient.getId();
    }

    @Override
    public Optional<Client> getClient(Long id) {
        Optional<Client> clientOptional = clientRepository.getClient(id);
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }
        return clientOptional;
    }

    @Override
    public List<Client> getAllClients(Pageable pageable) {
        return clientRepository.getAllClients(pageable);
    }

    @Override
    public void removeClient(Long id) {
        clientRepository.removeClient(id);
    }

    @Override
    public void updateClient(Long id, String name, String lastName, String email, Double discount, Double accountBalance) {

        validateUpdateClientRequest(id, name, lastName, email, discount, accountBalance);

        Client repositoryClient = clientRepository.updateClient(
                id,
                name,
                lastName,
                email,
                discount,
                accountBalance);
    }

    @Override
    public void transferToClientAccount(Long id, Double transferAmount) {

        validateTransferToClientAccountRequest(id, transferAmount);

        Double accountBalance = clientRepository.getClient(id).get().getAccountBalance() + transferAmount;
        clientRepository.transferToClientAccount(id, accountBalance);
    }

    @Override
    public List<Client> findClientsByFragmentOfEmail(String emailFragment, Pageable pageable) {
        return clientRepository.findClientsByFragmentOfEmail(emailFragment, pageable);
    }

//  ---------- Validators ----------

    private void validateCreateClientRequest(Client client) {

        if (ValidationUtils.isNullOrEmpty(client.getName())
                || ValidationUtils.isNullOrEmpty(client.getLastName())
                || ValidationUtils.isNullOrEmpty(client.getEmail())
                || ValidationUtils.isNull(client.getDiscount())
                || ValidationUtils.isNull(client.getAccountBalance())) {
            throw new CommonBadRequestException(msgSource.ERR014);
        }

        if (ValidationUtils.isUncorrectedEmail(client.getEmail())) {
            throw new CommonBadRequestException(msgSource.ERR015);
        }

        if (ValidationUtils.isUncorrectedDiscount(client.getDiscount())) {
            throw new CommonBadRequestException(msgSource.ERR016);
        }

        if (ValidationUtils.isUncorrectedAccountBalance(client.getAccountBalance())) {
            throw new CommonBadRequestException(msgSource.ERR017);
        }

        isEmailAlreadyExist(client.getEmail());
    }

    private void validateUpdateClientRequest(Long id, String name, String lastName, String email, Double discount, Double accountBalance) {

        if (ValidationUtils.isNull(id)
                || ValidationUtils.isNullOrEmpty(name)
                || ValidationUtils.isNullOrEmpty(lastName)
                || ValidationUtils.isNullOrEmpty(email)
                || ValidationUtils.isNull(discount)
                || ValidationUtils.isNull(accountBalance)) {
            throw new CommonBadRequestException(msgSource.ERR027);
        }

        if (ValidationUtils.isUncorrectedEmail(email)) {
            throw new CommonBadRequestException(msgSource.ERR015);
        }

        if (ValidationUtils.isUncorrectedDiscount(discount)) {
            throw new CommonBadRequestException(msgSource.ERR016);
        }

        if (ValidationUtils.isUncorrectedAccountBalance(accountBalance)) {
            throw new CommonBadRequestException(msgSource.ERR017);
        }

        Optional<Client> clientOptional = clientRepository.getClient(id);
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }

        isEmailAlreadyExist(email);
    }

    private void validateTransferToClientAccountRequest(Long id, Double transferAmount){

        if(ValidationUtils.isNull(id)
        || ValidationUtils.isNull(transferAmount)){
            throw new CommonBadRequestException(msgSource.ERR018);
        }

        if(ValidationUtils.isTransferAmountLessThanOrEqual0(transferAmount)){
            throw new CommonBadRequestException(msgSource.ERR019);
        }

        Optional<Client> clientOptional = clientRepository.getClient(id);
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }
    }

    private void isEmailAlreadyExist(String email){
        for (Client client : clientRepository.getAllClients(Pageable.unpaged())) {
            if(client.getEmail().equals(email)){
                throw new CommonBadRequestException(msgSource.ERR030);
            }
        }
    }
}
