package com.myproject.shop.domain.service.service;

import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.domain.model.Product;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ProductService {

    Long createProduct(Product product);

    Optional<Product> getProduct(Long id);

    List<Product> getAllProducts(Pageable pageable);

    void removeProduct(Long id);

    void updateProduct(Long id, String name, ECategory category, Double price, Integer amount);

    List<Product> findAllByCategory(ECategory category, Pageable pageable);

    List<Product> findProductsByFragmentOfName(String nameFragment, Pageable pageable);
}
