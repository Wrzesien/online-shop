package com.myproject.shop.domain.service.repository;

import com.myproject.shop.domain.model.Basket;
import com.myproject.shop.domain.model.EStatus;
import com.myproject.shop.infrastructure.entity.BasketHibernate;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import org.springframework.data.domain.Pageable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BasketRepository {

    Basket createBasket(
            Long clientId,
            Long parcelLockerId,
            LocalDateTime date,
            Double totalAmount,
            EStatus status,
            List<ProductHibernate> products);

    Basket addToBasket(
            Long basketId,
            Double totalAmount,
            List<ProductHibernate> products);

    Optional<Basket> getBasket(Long id);

    List<Basket> getAllBaskets(Pageable pageable);

    void removeBasket(Long id);

    Basket updateBasket(
            Long id,
            Long clientId,
            Long parcelLockerId,
            LocalDateTime date,
            Double totalAmount,
            EStatus status,
            List<ProductHibernate> products);

    Basket pay(
            Long id,
            Long parcelLockerId);

    Basket toDomain(BasketHibernate basketHibernate);
}
