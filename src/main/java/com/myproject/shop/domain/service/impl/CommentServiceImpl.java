package com.myproject.shop.domain.service.impl;

import com.myproject.shop.common.AbstractCommonService;
import com.myproject.shop.common.MsgSource;
import com.myproject.shop.common.ValidationUtils;
import com.myproject.shop.domain.model.Client;
import com.myproject.shop.domain.model.Comment;
import com.myproject.shop.domain.model.Product;
import com.myproject.shop.domain.service.repository.ClientRepository;
import com.myproject.shop.domain.service.repository.CommentRepository;
import com.myproject.shop.domain.service.repository.ProductRepository;
import com.myproject.shop.domain.service.service.CommentService;
import com.myproject.shop.exception.CommonBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl extends AbstractCommonService implements CommentService {

    private final CommentRepository commentRepository;
    private final ProductRepository productRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public CommentServiceImpl(MsgSource msgSource,
                              CommentRepository commentRepository,
                              ProductRepository productRepository,
                              ClientRepository clientRepository) {
        super(msgSource);
        this.commentRepository = commentRepository;
        this.productRepository = productRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public Long createComment(Comment comment) {

        validateCreateCommentRequest(comment);

        Comment repositoryComment = commentRepository.createComment(
                comment.getProductId(),
                comment.getClientId(),
                comment.getContent());

        return repositoryComment.getId();
    }

    @Override
    public Optional<Comment> getComment(Long id) {
        Optional<Comment> commentOptional = commentRepository.getComment(id);
        if (!commentOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR003);
        }
        return commentOptional;
    }

    @Override
    public List<Comment> getAllComments(Pageable pageable) {
        return commentRepository.getAllComments(pageable);
    }

    @Override
    public void removeComment(Long id) {
        commentRepository.removeComment(id);
    }

    @Override
    public void updateComment(Long id, Long productId, Long clientId, String content) {

        validateUpdateCommentRequest(id, productId, clientId, content);

        Comment repositoryComment = commentRepository.updateComment(
                id,
                productId,
                clientId,
                content);
    }

    @Override
    public List<Comment> findByClientId(Long id, Pageable pageable) {
        Optional<Client> clientOptional = clientRepository.getClient(id);
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }
        return commentRepository.findByClientId(id, pageable);
    }

    @Override
    public List<Comment> findByProductId(Long id, Pageable pageable) {
        Optional<Product> productOptional = productRepository.getProduct(id);
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }
        return commentRepository.findByProductId(id, pageable);
    }

//  ---------- Validators ----------

    private void validateCreateCommentRequest(Comment comment) {

        if (ValidationUtils.isNull(comment.getProductId())
                || ValidationUtils.isNull(comment.getClientId())
                || ValidationUtils.isNullOrEmpty(comment.getContent())) {
            throw new CommonBadRequestException(msgSource.ERR013);
        }

        Optional<Product> productOptional = productRepository.getProduct(comment.getProductId());
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }

        Optional<Client> clientOptional = clientRepository.getClient(comment.getClientId());
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }

        if (ValidationUtils.isNullOrEmpty(comment.getContent())) {
            throw new CommonBadRequestException(msgSource.ERR013);
        }
    }

    private void validateUpdateCommentRequest(Long id, Long productId, Long clientId, String content) {

        if (ValidationUtils.isNull(id)
                || ValidationUtils.isNull(productId)
                || ValidationUtils.isNull(clientId)
                || ValidationUtils.isNullOrEmpty(content)) {
            throw new CommonBadRequestException(msgSource.ERR026);
        }

        Optional<Comment> commentOptional = commentRepository.getComment(id);
        if (!commentOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR003);
        }

        Optional<Product> productOptional = productRepository.getProduct(productId);
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }

        Optional<Client> clientOptional = clientRepository.getClient(clientId);
        if (!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }
    }
}
