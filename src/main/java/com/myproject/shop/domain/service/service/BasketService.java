package com.myproject.shop.domain.service.service;

import com.myproject.shop.domain.model.Basket;
import com.myproject.shop.domain.model.EStatus;
import org.springframework.data.domain.Pageable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BasketService {

    Long createBasket(Long clientId, List<Long> productIdsList);

    void addToBasket(Long basketId, List<Long> productIdsList);

    Optional<Basket> getBasket(Long id);

    List<Basket> getAllBaskets(Pageable pageable);

    void removeBasket(Long id);

    void updateBasket(Long id, Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<Long> productIdsList);

    void pay(Long id, Long parcelLockerId);
}
