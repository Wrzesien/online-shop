package com.myproject.shop.domain.service.repository;

import com.myproject.shop.domain.model.Client;
import com.myproject.shop.infrastructure.entity.ClientHibernate;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ClientRepository {

    Client createClient(String name,
                        String lastName,
                        String email,
                        Double discount,
                        Double accountBalance);

    Optional<Client> getClient(Long id);

    List<Client> getAllClients(Pageable pageable);

    void removeClient (Long id);

    Client updateClient(Long id,
                        String name,
                        String lastName,
                        String email,
                        Double discount,
                        Double accountBalance);

    void transferToClientAccount(Long id, Double accountBalance);

    List<Client> findClientsByFragmentOfEmail(String emailFragment, Pageable pageable);

    Client toDomain(ClientHibernate clientHibernate);
}
