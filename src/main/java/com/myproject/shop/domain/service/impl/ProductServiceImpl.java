package com.myproject.shop.domain.service.impl;

import com.myproject.shop.common.AbstractCommonService;
import com.myproject.shop.common.MsgSource;
import com.myproject.shop.common.ValidationUtils;
import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.domain.model.Product;
import com.myproject.shop.domain.service.repository.ProductRepository;
import com.myproject.shop.domain.service.service.ProductService;
import com.myproject.shop.exception.CommonBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl extends AbstractCommonService implements ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(MsgSource msgSource, ProductRepository productRepository) {
        super(msgSource);
        this.productRepository = productRepository;
    }

    @Override
    public Long createProduct(Product product) {

        validateCreateProductRequest(product);

        Product repositoryProduct = productRepository.createProduct(
                product.getName(),
                product.getCategory(),
                product.getPrice(),
                product.getAmount());

        return repositoryProduct.getId();
    }

    @Override
    public Optional<Product> getProduct(Long id) {

        Optional<Product> productOptional = productRepository.getProduct(id);
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }
        return productOptional;
    }

    @Override
    public List<Product> getAllProducts(Pageable pageable) {
        return productRepository.getAllProducts(pageable);
    }

    @Override
    public void removeProduct(Long id) {
        productRepository.removeProduct(id);
    }

    @Override
    public void updateProduct(Long id, String name, ECategory category, Double price, Integer amount) {

        validateUpdateProductRequest(id, name, category, price, amount);

        Product repositoryProduct = productRepository.updateProduct(
                id,
                name,
                category,
                price,
                amount);
    }

    @Override
    public List<Product> findAllByCategory(ECategory category, Pageable pageable) {
        return productRepository.findAllByCategory(category, pageable);
    }

    @Override
    public List<Product> findProductsByFragmentOfName(String nameFragment, Pageable pageable) {
        return productRepository.findProductsByFragmentOfName(nameFragment, pageable);
    }

//  ---------- Validators ----------

    private void validateCreateProductRequest(Product product) {

        if (ValidationUtils.isNullOrEmpty(product.getName())
                || ValidationUtils.isNull(product.getCategory())
                || ValidationUtils.isNull(product.getPrice())
                || ValidationUtils.isNull(product.getAmount())) {
            throw new CommonBadRequestException(msgSource.ERR007);
        }

        if (ValidationUtils.isPriceLessThan0(product.getPrice())) {
            throw new CommonBadRequestException(msgSource.ERR008);
        }

        if (ValidationUtils.isAmountLessThan0(product.getAmount())) {
            throw new CommonBadRequestException(msgSource.ERR009);
        }

        isProductAlreadyExist(product.getName());
    }

    private void validateUpdateProductRequest(Long id, String name, ECategory category, Double price, Integer amount) {

        if (ValidationUtils.isNull(id)
                || ValidationUtils.isNullOrEmpty(name)
                || ValidationUtils.isNull(category)
                || ValidationUtils.isNull(price)
                || ValidationUtils.isNull(amount)) {
            throw new CommonBadRequestException(msgSource.ERR024);
        }

        if (ValidationUtils.isPriceLessThan0(price)) {
            throw new CommonBadRequestException(msgSource.ERR008);
        }

        if (ValidationUtils.isAmountLessThan0(amount)) {
            throw new CommonBadRequestException(msgSource.ERR009);
        }

        Optional<Product> productOptional = productRepository.getProduct(id);
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }

        isProductAlreadyExist(name);
    }

    private void isProductAlreadyExist(String name){
        for (Product product : productRepository.getAllProducts(Pageable.unpaged())) {
            if(product.getName().equals(name)){
                throw new CommonBadRequestException(msgSource.ERR031);
            }
        }
    }
}
