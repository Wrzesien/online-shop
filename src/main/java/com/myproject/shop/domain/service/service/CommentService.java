package com.myproject.shop.domain.service.service;

import com.myproject.shop.domain.model.Comment;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface CommentService {

    Long createComment(Comment comment);

    Optional<Comment> getComment(Long id);

    List<Comment> getAllComments(Pageable pageable);

    void removeComment(Long id);

    void updateComment(Long id, Long productId, Long clientId, String content);

    List<Comment> findByClientId(Long id, Pageable pageable);

    List<Comment> findByProductId(Long id, Pageable pageable);
}
