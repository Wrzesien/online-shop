package com.myproject.shop.domain.service.impl;

import com.myproject.shop.common.AbstractCommonService;
import com.myproject.shop.common.MsgSource;
import com.myproject.shop.common.ValidationUtils;
import com.myproject.shop.domain.model.ParcelLocker;
import com.myproject.shop.domain.service.repository.ParcelLockerRepository;
import com.myproject.shop.domain.service.service.ParcelLockerService;
import com.myproject.shop.exception.CommonBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ParcelLockerServiceImpl extends AbstractCommonService implements ParcelLockerService {

    private final ParcelLockerRepository parcelLockerRepository;

    @Autowired
    public ParcelLockerServiceImpl(MsgSource msgSource, ParcelLockerRepository parcelLockerRepository) {
        super(msgSource);
        this.parcelLockerRepository = parcelLockerRepository;
    }

    @Override
    public Long createParcelLocker(ParcelLocker parcelLocker) {

        validateCreateParcelLockerRequest(parcelLocker);

        ParcelLocker repositoryParcelLocker = parcelLockerRepository.createParcelLocker(
                parcelLocker.getAddress(),
                parcelLocker.getLongitude(),
                parcelLocker.getLatitude());

        return repositoryParcelLocker.getId();
    }

    @Override
    public Optional<ParcelLocker> getParcelLocker(Long id) {
        Optional<ParcelLocker> parcelLockerOptional = parcelLockerRepository.getParcelLocker(id);
        if (!parcelLockerOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR001);
        }
        return parcelLockerOptional;
    }

    @Override
    public List<ParcelLocker> getAllParcelLockers(Pageable pageable) {
        return parcelLockerRepository.getAllParcelLockers(pageable);
    }

    @Override
    public void updateParcelLocker(Long id, String address, Double longitude, Double latitude) {

        validateUpdateParcelLockerRequest(id, address, longitude, latitude);

        ParcelLocker repositoryParcelLocker = parcelLockerRepository.updateParcelLocker(
                id,
                address,
                longitude,
                latitude);
    }

    @Override
    public void removeParcelLocker(Long id) {
        parcelLockerRepository.removeParcelLocker(id);
    }

//  ---------- Validators ----------

    private void validateCreateParcelLockerRequest(ParcelLocker parcelLocker) {

        if (ValidationUtils.isNullOrEmpty(parcelLocker.getAddress())
                || ValidationUtils.isNull(parcelLocker.getLongitude())
                || ValidationUtils.isNull(parcelLocker.getLatitude())) {
            throw new CommonBadRequestException(msgSource.ERR010);
        }

        if (ValidationUtils.isUncorrectedLongitude(parcelLocker.getLongitude())) {
            throw new CommonBadRequestException(msgSource.ERR011);
        }

        if (ValidationUtils.isUncorrectedLatitude(parcelLocker.getLatitude())) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }
    }

    private void validateUpdateParcelLockerRequest(Long id, String address, Double longitude, Double latitude) {

        if (ValidationUtils.isNull(id)
                || ValidationUtils.isNullOrEmpty(address)
                || ValidationUtils.isNull(longitude)
                || ValidationUtils.isNull(latitude)) {
            throw new CommonBadRequestException(msgSource.ERR025);
        }

        if (ValidationUtils.isUncorrectedLongitude(longitude)) {
            throw new CommonBadRequestException(msgSource.ERR011);
        }

        if (ValidationUtils.isUncorrectedLatitude(latitude)) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }

        Optional<ParcelLocker> parcelLockerOptional = parcelLockerRepository.getParcelLocker(id);
        if (!parcelLockerOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR001);
        }
    }
}
