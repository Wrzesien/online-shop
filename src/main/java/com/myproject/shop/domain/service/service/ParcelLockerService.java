package com.myproject.shop.domain.service.service;

import com.myproject.shop.domain.model.ParcelLocker;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ParcelLockerService {

    Long createParcelLocker(ParcelLocker parcelLocker);

    Optional<ParcelLocker> getParcelLocker(Long id);

    List<ParcelLocker> getAllParcelLockers(Pageable pageable);

    void updateParcelLocker(Long id, String address, Double longitude, Double latitude);

    void removeParcelLocker(Long id);

}
