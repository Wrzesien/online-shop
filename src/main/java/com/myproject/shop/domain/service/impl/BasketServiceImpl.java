package com.myproject.shop.domain.service.impl;

import com.myproject.shop.common.AbstractCommonService;
import com.myproject.shop.common.MsgSource;
import com.myproject.shop.common.ValidationUtils;
import com.myproject.shop.domain.model.*;
import com.myproject.shop.domain.service.repository.BasketRepository;
import com.myproject.shop.domain.service.repository.ClientRepository;
import com.myproject.shop.domain.service.repository.ParcelLockerRepository;
import com.myproject.shop.domain.service.repository.ProductRepository;
import com.myproject.shop.domain.service.service.BasketService;
import com.myproject.shop.exception.CommonBadRequestException;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import com.myproject.shop.infrastructure.repository.ProductHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BasketServiceImpl extends AbstractCommonService implements BasketService {

    private BasketRepository basketRepository;
    private ProductRepository productRepository;
    private ClientRepository clientRepository;
    private ParcelLockerRepository parcelLockerRepository;
    private ProductHibernateRepository productHibernateRepository;

    @Autowired
    public BasketServiceImpl(MsgSource msgSource,
                             BasketRepository basketRepository,
                             ProductRepository productRepository,
                             ClientRepository clientRepository,
                             ParcelLockerRepository parcelLockerRepository,
                             ProductHibernateRepository productHibernateRepository) {
        super(msgSource);
        this.basketRepository = basketRepository;
        this.productRepository = productRepository;
        this.clientRepository = clientRepository;
        this.parcelLockerRepository = parcelLockerRepository;
        this.productHibernateRepository = productHibernateRepository;
    }

    @Override
    @Transactional
    public Long createBasket(Long clientId, List<Long> productIdsList) {

        validateCreateBasketRequest(clientId, productIdsList);

        removeProductsFromStock(productIdsList);
        Double totalAmount = sumProductsInTheBasket(productIdsList) * (1 - clientRepository.getClient(clientId).get().getDiscount());

        Basket repositoryBasket = basketRepository.createBasket(
                clientId,
                1l,
                null,
                totalAmount,
                EStatus.STARTED,
                extractProductsFromIdsList(productIdsList));

        return repositoryBasket.getId();
    }

    @Override
    @Transactional
    public void addToBasket(Long basketId, List<Long> productIdsList){

        validateAddToBasketRequest(basketId, productIdsList);

        removeProductsFromStock(productIdsList);

        Client client = clientRepository.getClient(basketRepository.getBasket(basketId).get().getClientId()).get();
        Double totalAmount = basketRepository.getBasket(basketId).get().getTotalAmount() +
                sumProductsInTheBasket(productIdsList) * (1 - client.getDiscount());
        List<ProductHibernate> newProducts = extractProductsFromIdsList(productIdsList);

        basketRepository.addToBasket(basketId, totalAmount, newProducts);
    }

    @Override
    public Optional<Basket> getBasket(Long id) {
        Optional<Basket> basketOptional = basketRepository.getBasket(id);
        if(!basketOptional.isPresent()){
            throw new CommonBadRequestException(msgSource.ERR005);
        }
        return basketOptional;
    }

    @Override
    public List<Basket> getAllBaskets(Pageable pageable) {
        return basketRepository.getAllBaskets(pageable);
    }

    @Override
    public void removeBasket(Long id) {
        basketRepository.removeBasket(id);
    }

    @Override
    public void updateBasket(Long id, Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<Long> productIdsList) {

        validateUpdateBasketRequest(id,clientId, parcelLockerId, date, totalAmount, status, productIdsList);

        Basket repositoryBasket = basketRepository.updateBasket(
                id,
                clientId,
                parcelLockerId,
                date,
                totalAmount,
                status,
                extractProductsFromIdsList(productIdsList));
    }

    @Override
    @Transactional
    public void pay(Long id, Long parcelLockerId){

        validatePayRequest(id, parcelLockerId);

        getFundsFromCustomerAccount(id);

        Basket repositoryBasket = basketRepository.pay(id, parcelLockerId);
    }

    private Double sumProductsInTheBasket(List<Long> productIdsList) {
        Double sum = 0d;
        for (Long id : productIdsList) {
            ProductHibernate productHibernate = productHibernateRepository.findById(id).get();
            sum += productHibernate.getPrice();
        }
        return sum;
    }

    List<ProductHibernate> extractProductsFromIdsList(List<Long> productIdsList){
        List<ProductHibernate> products = new ArrayList<>();
        for (Long id : productIdsList) {
            ProductHibernate productHibernate = productHibernateRepository.findById(id).get();
            products.add(productHibernate);
        }
        return products;
    }

    private void removeProductsFromStock(List<Long> productIdsList) {
        for (Long id : productIdsList) {
            Product repositoryProduct = productRepository.getProduct(id).get();
            Integer newAmount = repositoryProduct.getAmount() - 1;
            if(newAmount < 0){
                throw new CommonBadRequestException(msgSource.ERR029);
            }
            productRepository.updateProduct(
                    repositoryProduct.getId(),
                    repositoryProduct.getName(),
                    repositoryProduct.getCategory(),
                    repositoryProduct.getPrice(),
                    newAmount);
        }
    }

    private void getFundsFromCustomerAccount(Long id) {
        Double amountToPay = basketRepository.getBasket(id).get().getTotalAmount();
        Client client = clientRepository.getClient(basketRepository.getBasket(id).get().getClientId()).get();
        Double newClientAccountBalance = client.getAccountBalance() - amountToPay;
        if(newClientAccountBalance < 0){
            throw new CommonBadRequestException(msgSource.ERR006);
        }
        Client repositoryClient = clientRepository.updateClient(
                client.getId(),
                client.getName(),
                client.getLastName(),
                client.getEmail(),
                client.getDiscount(),
                newClientAccountBalance);
    }

//  ---------- Validators ----------

    private void validateCreateBasketRequest(Long clientId, List<Long> productIdsList){

        if(ValidationUtils.isNull(clientId)
        || ValidationUtils.isNull(productIdsList)){
            throw new CommonBadRequestException(msgSource.ERR020);
        }

        Optional<Client> clientOptional = clientRepository.getClient(clientId);
        if(!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }

        areProductsExist(productIdsList);
    }

    private void validateAddToBasketRequest(Long basketId, List<Long> productIdsList){

        if(ValidationUtils.isNull(basketId)
        || ValidationUtils.isNull(productIdsList)){
            throw new CommonBadRequestException(msgSource.ERR021);
        }

        Optional<Basket> basketOptional = basketRepository.getBasket(basketId);
        if(!basketOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR005);
        }

        areProductsExist(productIdsList);
    }

    private void validateUpdateBasketRequest(Long id, Long clientId, Long parcelLockerId, LocalDateTime date,
                                             Double totalAmount, EStatus status, List<Long> productIdsList){
        if(ValidationUtils.isNull(id)
        || ValidationUtils.isNull(clientId)
        || ValidationUtils.isNull(parcelLockerId)
        || ValidationUtils.isNull(date)
        || ValidationUtils.isNull(totalAmount)
        || ValidationUtils.isNull(status)
        || ValidationUtils.isNull(productIdsList)){
            throw new CommonBadRequestException(msgSource.ERR028);
        }

        if(ValidationUtils.isAmountLessThan0(totalAmount)){
            throw new CommonBadRequestException(msgSource.ERR022);
        }

        Optional<Basket> basketOptional = basketRepository.getBasket(id);
        if(!basketOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR005);
        }
        Optional<Client> clientOptional = clientRepository.getClient(clientId);
        if(!clientOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR002);
        }
        Optional<ParcelLocker> parcelLockerOptional = parcelLockerRepository.getParcelLocker(parcelLockerId);
        if(!parcelLockerOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR001);
        }

        areProductsExist(productIdsList);
    }

    private void validatePayRequest(Long id, Long parcelLockerId){

        if(ValidationUtils.isNull(id)
        || ValidationUtils.isNull(parcelLockerId)){
            throw new CommonBadRequestException(msgSource.ERR023);
        }

        Optional<Basket> basketOptional = basketRepository.getBasket(id);
        if(!basketOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR005);
        }
        Optional<ParcelLocker> parcelLockerOptional = parcelLockerRepository.getParcelLocker(parcelLockerId);
        if(!parcelLockerOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR001);
        }
    }

    private void areProductsExist(List<Long> productIdsList){
        for (Long id : productIdsList) {
            Optional<Product> productOptional = productRepository.getProduct(id);
            if(!productOptional.isPresent()){
                throw new CommonBadRequestException(msgSource.ERR004);
            }
        }
    }
}
