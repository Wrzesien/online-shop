package com.myproject.shop.domain.service.service;

import com.myproject.shop.domain.model.Client;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ClientService {

    Long createClient(Client client);

    Optional<Client> getClient(Long id);

    List<Client> getAllClients(Pageable pageable);

    void removeClient(Long id);

    void updateClient(Long id, String name, String lastName, String email, Double discount, Double accountBalance);

    void transferToClientAccount(Long id, Double transferAmount);

    List<Client> findClientsByFragmentOfEmail(String emailFragment, Pageable pageable);
}
