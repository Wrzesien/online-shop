package com.myproject.shop.domain.service.repository;

import com.myproject.shop.domain.model.Comment;
import com.myproject.shop.infrastructure.entity.CommentHibernate;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface CommentRepository {

    Comment createComment(Long productId,
                          Long clientId,
                          String content);

    Optional<Comment> getComment(Long id);

    List<Comment> getAllComments(Pageable pageable);

    void removeComment(Long id);

    Comment updateComment(Long id,
                          Long productId,
                          Long clientId,
                          String content);

    List<Comment> findByClientId(Long id, Pageable pageable);

    List<Comment> findByProductId(Long id, Pageable pageable);

    Comment toDomain(CommentHibernate commentHibernate);
}
