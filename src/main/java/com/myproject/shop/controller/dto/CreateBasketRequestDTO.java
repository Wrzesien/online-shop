package com.myproject.shop.controller.dto;

import java.io.Serializable;
import java.util.List;

public class CreateBasketRequestDTO implements Serializable {

    private Long clientId;
    private List<Long> productIdsList;

    public CreateBasketRequestDTO() {
    }

    public CreateBasketRequestDTO(Long clientId, List<Long> productIdsList) {
        this.clientId = clientId;
        this.productIdsList = productIdsList;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public List<Long> getProductIdsList() {
        return productIdsList;
    }

    public void setProductIdsList(List<Long> productIdsList) {
        this.productIdsList = productIdsList;
    }
}
