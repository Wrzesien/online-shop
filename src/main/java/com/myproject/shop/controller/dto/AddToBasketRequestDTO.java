package com.myproject.shop.controller.dto;

import java.io.Serializable;
import java.util.List;

public class AddToBasketRequestDTO implements Serializable {

    private Long basketId;
    private List<Long> productIdsList;

    public AddToBasketRequestDTO() {
    }

    public AddToBasketRequestDTO(Long basketId, List<Long> productIdsList) {
        this.basketId = basketId;
        this.productIdsList = productIdsList;
    }

    public Long getBasketId() {
        return basketId;
    }

    public void setBasketId(Long basketId) {
        this.basketId = basketId;
    }

    public List<Long> getProductIdsList() {
        return productIdsList;
    }

    public void setProductIdsList(List<Long> productIdsList) {
        this.productIdsList = productIdsList;
    }
}
