package com.myproject.shop.controller.dto;

import com.myproject.shop.domain.model.ParcelLocker;
import java.io.Serializable;

public class ParcelLockerDTO implements Serializable {

    private Long id;
    private String address;
    private Double longitude;
    private Double latitude;

    public ParcelLockerDTO() {
    }

    public ParcelLockerDTO(Long id, String address, Double longitude, Double latitude) {
        this.id = id;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public ParcelLocker toDomain(){
        return new ParcelLocker(id, address, longitude, latitude);
    }

    public static ParcelLockerDTO toDTO(ParcelLocker parcelLocker){
        return new ParcelLockerDTO(
                parcelLocker.getId(),
                parcelLocker.getAddress(),
                parcelLocker.getLongitude(),
                parcelLocker.getLatitude());
    }
}
