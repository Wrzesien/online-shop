package com.myproject.shop.controller.dto;

import com.myproject.shop.domain.model.Client;
import java.io.Serializable;

public class ClientDTO implements Serializable {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private Double discount;
    private Double accountBalance;

    public ClientDTO() {
    }

    public ClientDTO(Long id, String name, String lastName, String email, Double discount, Double accountBalance) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.discount = discount;
        this.accountBalance = accountBalance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Client toDomain() {
        return new Client(id, name, lastName, email, discount, accountBalance);
    }

    public static ClientDTO toDTO(Client client){
        return new ClientDTO(
                client.getId(),
                client.getName(),
                client.getLastName(),
                client.getEmail(),
                client.getDiscount(),
                client.getAccountBalance());
    }
}
