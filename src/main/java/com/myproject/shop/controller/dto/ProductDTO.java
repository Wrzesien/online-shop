package com.myproject.shop.controller.dto;

import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.domain.model.Product;
import java.io.Serializable;

public class ProductDTO implements Serializable {

    private Long id;
    private String name;
    private ECategory category;
    private Double price;
    private Integer amount;

    public ProductDTO() {
    }

    public ProductDTO(Long id, String name, ECategory category, Double price, Integer amount) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ECategory getCategory() {
        return category;
    }

    public void setCategory(ECategory category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Product toDomain(){
        return new Product(id, name, category, price, amount);
    }

    public static ProductDTO toDTO(Product product){
        return new ProductDTO(
                product.getId(),
                product.getName(),
                product.getCategory(),
                product.getPrice(),
                product.getAmount());
    }
}
