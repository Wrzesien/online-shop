package com.myproject.shop.controller.controller;

import com.myproject.shop.controller.dto.ClientDTO;
import com.myproject.shop.domain.model.Client;
import com.myproject.shop.domain.service.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("client")
public class ClientController {

    private ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping(value = "/add")
    public Long createClient(@RequestBody ClientDTO clientDTO) {
        return clientService.createClient(clientDTO.toDomain());
    }

    @GetMapping(value = "/get/{id}")
    public ClientDTO getClient(@PathVariable("id") Long id) {
        Optional<Client> clientOptional = clientService.getClient(id);
        return ClientDTO.toDTO(clientOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<ClientDTO> getAllClients() {
        List<Client> clients = clientService.getAllClients(Pageable.unpaged());
        return clients.stream().map(ClientDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.removeClient(id);
    }

    @PutMapping(value = "/update")
    public void updateClient(@RequestBody ClientDTO clientDTO){
        clientService.updateClient(
                clientDTO.getId(),
                clientDTO.getName(),
                clientDTO.getLastName(),
                clientDTO.getEmail(),
                clientDTO.getDiscount(),
                clientDTO.getAccountBalance());
    }

    @PutMapping(value = "/transferAmount/{id}")
    public void transferToClientAccount(@PathVariable("id") Long id, @PathParam("transferAmount") Double transferAmount){
        clientService.transferToClientAccount(id, transferAmount);
    }

    @GetMapping(value = "/getClientsByFragmentOfEmail")
    public List<ClientDTO> getClientsByFragmentOfEmail(@PathParam("emailFragment") String emailFragment){
        List<Client> clientsByEmail = clientService.findClientsByFragmentOfEmail(emailFragment, Pageable.unpaged());
        return clientsByEmail.stream().map(ClientDTO::toDTO).collect(Collectors.toList());
    }
}
