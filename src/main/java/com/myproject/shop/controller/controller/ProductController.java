package com.myproject.shop.controller.controller;

import com.myproject.shop.controller.dto.ProductDTO;
import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.domain.model.Product;
import com.myproject.shop.domain.service.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/add")
    public Long createProduct(@RequestBody ProductDTO productDTO){
        return productService.createProduct(productDTO.toDomain());
    }

    @GetMapping(value = "/get/{id}")
    public ProductDTO getProduct(@PathVariable("id") Long id){
        Optional<Product> productOptional = productService.getProduct(id);
        return ProductDTO.toDTO(productOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<ProductDTO> getAllProducts(){
        List<Product> products = productService.getAllProducts(Pageable.unpaged());
        return products.stream().map(ProductDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteProduct(@PathVariable("id") Long id){
        productService.removeProduct(id);
    }

    @PutMapping(value = "/update")
    public void updateProduct(@RequestBody ProductDTO productDTO){
        productService.updateProduct(
                productDTO.getId(),
                productDTO.getName(),
                productDTO.getCategory(),
                productDTO.getPrice(),
                productDTO.getAmount());
    }

    @GetMapping(value = "/getProductsInCategory")
    public List<ProductDTO> getProductsInCategory(@PathParam("category") ECategory category){
        List<Product> products = productService.findAllByCategory(category, Pageable.unpaged());
        return products.stream().map(ProductDTO::toDTO).collect(Collectors.toList());
    }

    @GetMapping(value = "/getProductsByFragmentOfName")
    public List<ProductDTO> getProductsByFragmentOfName(@PathParam("nameFragment") String nameFragment){
        List<Product> products = productService.findProductsByFragmentOfName(nameFragment, Pageable.unpaged());
        return products.stream().map(ProductDTO::toDTO).collect(Collectors.toList());
    }
}
