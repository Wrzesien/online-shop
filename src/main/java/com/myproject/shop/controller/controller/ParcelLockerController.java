package com.myproject.shop.controller.controller;

import com.myproject.shop.controller.dto.ParcelLockerDTO;
import com.myproject.shop.domain.model.ParcelLocker;
import com.myproject.shop.domain.service.service.ParcelLockerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("parcel_locker")
public class ParcelLockerController {

    private ParcelLockerService parcelLockerService;

    @Autowired
    public ParcelLockerController(ParcelLockerService parcelLockerService) {
        this.parcelLockerService = parcelLockerService;
    }

    @PostMapping(value = "/add")
    public Long createParcelLocker(@RequestBody ParcelLockerDTO parcelLockerDTO) {
        return parcelLockerService.createParcelLocker(parcelLockerDTO.toDomain());
    }

    @GetMapping(value = "/get/{id}")
    public ParcelLockerDTO getParcelLocker(@PathVariable("id") Long id) {
        Optional<ParcelLocker> parcelLockerOptional = parcelLockerService.getParcelLocker(id);
        return ParcelLockerDTO.toDTO(parcelLockerOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<ParcelLockerDTO> getAllParcelLockers() {
        List<ParcelLocker> parcelLockers = parcelLockerService.getAllParcelLockers(Pageable.unpaged());
        return parcelLockers.stream().map(ParcelLockerDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteParcelLocker(@PathVariable("id") Long id){
        parcelLockerService.removeParcelLocker(id);
    }

    @PutMapping(value = "/update")
    public void updateParcelLocker(@RequestBody ParcelLockerDTO parcelLockerDTO){
        parcelLockerService.updateParcelLocker(
                parcelLockerDTO.getId(),
                parcelLockerDTO.getAddress(),
                parcelLockerDTO.getLongitude(),
                parcelLockerDTO.getLatitude());
    }
}
