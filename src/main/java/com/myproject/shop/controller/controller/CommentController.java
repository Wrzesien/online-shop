package com.myproject.shop.controller.controller;

import com.myproject.shop.controller.dto.CommentDTO;
import com.myproject.shop.domain.model.Comment;
import com.myproject.shop.domain.service.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("comment")
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping(value = "/add")
    public Long createComment(@RequestBody CommentDTO commentDTO){
        return commentService.createComment(commentDTO.toDomain());
    }

    @GetMapping(value = "/get/{id}")
    public CommentDTO getComment(@PathVariable("id") Long id){
        Optional<Comment> commentOptional = commentService.getComment(id);
        return CommentDTO.toDTO(commentOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<CommentDTO> getAllComments() {
        List<Comment> comments = commentService.getAllComments(Pageable.unpaged());
        return comments.stream().map(CommentDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteComment(@PathVariable("id") Long id){
        commentService.removeComment(id);
    }

    @PutMapping(value = "/update")
    public void updateComment(@RequestBody CommentDTO commentDTO){
        commentService.updateComment(
                commentDTO.getId(),
                commentDTO.getProductId(),
                commentDTO.getClientId(),
                commentDTO.getContent());
    }

    @GetMapping(value = "/getByClientId/{id}")
    public List<CommentDTO> getCommentsByClientId(@PathVariable("id") Long id) {
        List<Comment> commentsByClientId = commentService.findByClientId(id, Pageable.unpaged());
        return commentsByClientId.stream().map(CommentDTO::toDTO).collect(Collectors.toList());
    }

    @GetMapping(value = "/getByProductId/{id}")
    public List<CommentDTO> getCommentsByProductId(@PathVariable("id") Long id){
        List<Comment> commentsByProductId = commentService.findByProductId(id, Pageable.unpaged());
        return commentsByProductId.stream().map(CommentDTO::toDTO).collect(Collectors.toList());
    }
}
