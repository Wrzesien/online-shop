package com.myproject.shop.controller.controller;

import com.myproject.shop.controller.dto.AddToBasketRequestDTO;
import com.myproject.shop.controller.dto.BasketDTO;
import com.myproject.shop.controller.dto.CreateBasketRequestDTO;
import com.myproject.shop.controller.dto.UpdateBasketRequestDTO;
import com.myproject.shop.domain.model.Basket;
import com.myproject.shop.domain.service.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("basket")
public class BasketController {

    private BasketService basketService;

    @Autowired
    public BasketController(BasketService basketService) {
        this.basketService = basketService;
    }

    @PostMapping(value = "/add")
    public Long createBasket(@RequestBody CreateBasketRequestDTO cbrDTO) {
        return basketService.createBasket(
                cbrDTO.getClientId(),
                cbrDTO.getProductIdsList());
    }

    @PutMapping(value = "/addToBasket")
    public void addToBasket(@RequestBody AddToBasketRequestDTO abrDTO) {
        basketService.addToBasket(
                abrDTO.getBasketId(),
                abrDTO.getProductIdsList());
    }

    @GetMapping(value = "/get/{id}")
    public BasketDTO getBasket(@PathVariable("id") Long id) {
        Optional<Basket> basketOptional = basketService.getBasket(id);
        return BasketDTO.toDTO(basketOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<BasketDTO> getAllBaskets() {
        List<Basket> baskets = basketService.getAllBaskets(Pageable.unpaged());
        return baskets.stream().map(BasketDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteBasket(@PathVariable("id") Long id) {
        basketService.removeBasket(id);
    }

    @PutMapping(value = "/update")
    public void updateBasket(@RequestBody UpdateBasketRequestDTO ubrDTO) {
        basketService.updateBasket(
                ubrDTO.getId(),
                ubrDTO.getClientId(),
                ubrDTO.getParcelLockerId(),
                ubrDTO.getDate(),
                ubrDTO.getTotalAmount(),
                ubrDTO.getStatus(),
                ubrDTO.getProducts());
    }

    @PutMapping(value = "/pay/{id}")
    public void pay(@PathVariable("id") Long id, @PathParam("parcelLockerId") Long parcelLockerId) {
        basketService.pay(id, parcelLockerId);
    }
}
