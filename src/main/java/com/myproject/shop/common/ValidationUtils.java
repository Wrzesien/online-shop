package com.myproject.shop.common;

import java.util.regex.Pattern;

public class ValidationUtils {

    private static final Double MIN_LONGITUDE = -180d;
    private static final Double MAX_LONGITUDE = 180d;
    private static final Double MIN_LATITUDE = -90d;
    private static final Double MAX_LATITUDE = 90d;
    private static final Double MIN_DISCOUNT = 0d;
    private static final Double MAX_DISCOUNT = 1d;
    private static Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/-?~^.]+@[a-zA-Z0-9.-]+$");

    public static Boolean isNullOrEmpty(String value){
        return value == null || value.isEmpty();
    }

    public static Boolean isNull(Object object){
        return object == null;
    }

    public static Boolean isPriceLessThan0(Double price){
        return price < 0;
    }

    public static Boolean isAmountLessThan0(Integer amount){
        return amount < 0;
    }

    public static Boolean isAmountLessThan0(Double totalAmount){
        return totalAmount < 0;
    }

    public static Boolean isTransferAmountLessThanOrEqual0(Double transferAmount){
        return transferAmount <= 0;
    }

    public static Boolean isUncorrectedEmail(String email) {
        return !emailPattern.matcher(email).matches();
    }

    public static Boolean isUncorrectedLongitude(Double longitude){
        return longitude < MIN_LONGITUDE || longitude > MAX_LONGITUDE;
    }

    public static Boolean isUncorrectedLatitude(Double latitude){
        return latitude < MIN_LATITUDE || latitude > MAX_LATITUDE;
    }

    public static Boolean isUncorrectedDiscount(Double discount){
        return discount < MIN_DISCOUNT || discount > MAX_DISCOUNT;
    }

    public static Boolean isUncorrectedAccountBalance(Double accountBalance){
        return accountBalance < 0;
    }
}


