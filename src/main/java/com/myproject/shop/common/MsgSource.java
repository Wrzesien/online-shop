package com.myproject.shop.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MsgSource {

    public final ConstErrorMsg ERR001;
    public final ConstErrorMsg ERR002;
    public final ConstErrorMsg ERR003;
    public final ConstErrorMsg ERR004;
    public final ConstErrorMsg ERR005;
    public final ConstErrorMsg ERR006;
    public final ConstErrorMsg ERR007;
    public final ConstErrorMsg ERR008;
    public final ConstErrorMsg ERR009;
    public final ConstErrorMsg ERR010;
    public final ConstErrorMsg ERR011;
    public final ConstErrorMsg ERR012;
    public final ConstErrorMsg ERR013;
    public final ConstErrorMsg ERR014;
    public final ConstErrorMsg ERR015;
    public final ConstErrorMsg ERR016;
    public final ConstErrorMsg ERR017;
    public final ConstErrorMsg ERR018;
    public final ConstErrorMsg ERR019;
    public final ConstErrorMsg ERR020;
    public final ConstErrorMsg ERR021;
    public final ConstErrorMsg ERR022;
    public final ConstErrorMsg ERR023;
    public final ConstErrorMsg ERR024;
    public final ConstErrorMsg ERR025;
    public final ConstErrorMsg ERR026;
    public final ConstErrorMsg ERR027;
    public final ConstErrorMsg ERR028;
    public final ConstErrorMsg ERR029;
    public final ConstErrorMsg ERR030;
    public final ConstErrorMsg ERR031;

    public MsgSource(
            @Value("${common.const.error.msg.err001}") String err001MsgValue,
            @Value("${common.const.error.msg.err002}") String err002MsgValue,
            @Value("${common.const.error.msg.err003}") String err003MsgValue,
            @Value("${common.const.error.msg.err004}") String err004MsgValue,
            @Value("${common.const.error.msg.err005}") String err005MsgValue,
            @Value("${common.const.error.msg.err006}") String err006MsgValue,
            @Value("${common.const.error.msg.err007}") String err007MsgValue,
            @Value("${common.const.error.msg.err008}") String err008MsgValue,
            @Value("${common.const.error.msg.err009}") String err009MsgValue,
            @Value("${common.const.error.msg.err010}") String err010MsgValue,
            @Value("${common.const.error.msg.err011}") String err011MsgValue,
            @Value("${common.const.error.msg.err012}") String err012MsgValue,
            @Value("${common.const.error.msg.err013}") String err013MsgValue,
            @Value("${common.const.error.msg.err014}") String err014MsgValue,
            @Value("${common.const.error.msg.err015}") String err015MsgValue,
            @Value("${common.const.error.msg.err016}") String err016MsgValue,
            @Value("${common.const.error.msg.err017}") String err017MsgValue,
            @Value("${common.const.error.msg.err018}") String err018MsgValue,
            @Value("${common.const.error.msg.err019}") String err019MsgValue,
            @Value("${common.const.error.msg.err020}") String err020MsgValue,
            @Value("${common.const.error.msg.err021}") String err021MsgValue,
            @Value("${common.const.error.msg.err022}") String err022MsgValue,
            @Value("${common.const.error.msg.err023}") String err023MsgValue,
            @Value("${common.const.error.msg.err024}") String err024MsgValue,
            @Value("${common.const.error.msg.err025}") String err025MsgValue,
            @Value("${common.const.error.msg.err026}") String err026MsgValue,
            @Value("${common.const.error.msg.err027}") String err027MsgValue,
            @Value("${common.const.error.msg.err028}") String err028MsgValue,
            @Value("${common.const.error.msg.err029}") String err029MsgValue,
            @Value("${common.const.error.msg.err030}") String err030MsgValue,
            @Value("${common.const.error.msg.err031}") String err031MsgValue
    ) {
        ERR001 = new ConstErrorMsg("ERR001", err001MsgValue);
        ERR002 = new ConstErrorMsg("ERR002", err002MsgValue);
        ERR003 = new ConstErrorMsg("ERR003", err003MsgValue);
        ERR004 = new ConstErrorMsg("ERR004", err004MsgValue);
        ERR005 = new ConstErrorMsg("ERR005", err005MsgValue);
        ERR006 = new ConstErrorMsg("ERR006", err006MsgValue);
        ERR007 = new ConstErrorMsg("ERR007", err007MsgValue);
        ERR008 = new ConstErrorMsg("ERR008", err008MsgValue);
        ERR009 = new ConstErrorMsg("ERR009", err009MsgValue);
        ERR010 = new ConstErrorMsg("ERR010", err010MsgValue);
        ERR011 = new ConstErrorMsg("ERR011", err011MsgValue);
        ERR012 = new ConstErrorMsg("ERR012", err012MsgValue);
        ERR013 = new ConstErrorMsg("ERR013", err013MsgValue);
        ERR014 = new ConstErrorMsg("ERR014", err014MsgValue);
        ERR015 = new ConstErrorMsg("ERR015", err015MsgValue);
        ERR016 = new ConstErrorMsg("ERR016", err016MsgValue);
        ERR017 = new ConstErrorMsg("ERR017", err017MsgValue);
        ERR018 = new ConstErrorMsg("ERR018", err018MsgValue);
        ERR019 = new ConstErrorMsg("ERR019", err019MsgValue);
        ERR020 = new ConstErrorMsg("ERR020", err020MsgValue);
        ERR021 = new ConstErrorMsg("ERR021", err021MsgValue);
        ERR022 = new ConstErrorMsg("ERR022", err022MsgValue);
        ERR023 = new ConstErrorMsg("ERR023", err023MsgValue);
        ERR024 = new ConstErrorMsg("ERR024", err024MsgValue);
        ERR025 = new ConstErrorMsg("ERR025", err025MsgValue);
        ERR026 = new ConstErrorMsg("ERR026", err026MsgValue);
        ERR027 = new ConstErrorMsg("ERR027", err027MsgValue);
        ERR028 = new ConstErrorMsg("ERR028", err028MsgValue);
        ERR029 = new ConstErrorMsg("ERR029", err029MsgValue);
        ERR030 = new ConstErrorMsg("ERR030", err030MsgValue);
        ERR031 = new ConstErrorMsg("ERR031", err031MsgValue);
    }
}
