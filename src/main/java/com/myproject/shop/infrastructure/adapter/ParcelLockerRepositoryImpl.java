package com.myproject.shop.infrastructure.adapter;

import com.myproject.shop.domain.model.ParcelLocker;
import com.myproject.shop.domain.service.repository.ParcelLockerRepository;
import com.myproject.shop.infrastructure.entity.ParcelLockerHibernate;
import com.myproject.shop.infrastructure.repository.ParcelLockerHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ParcelLockerRepositoryImpl implements ParcelLockerRepository {

    private ParcelLockerHibernateRepository parcelLockerHibernateRepository;

    @Autowired
    public ParcelLockerRepositoryImpl(ParcelLockerHibernateRepository parcelLockerHibernateRepository) {
        this.parcelLockerHibernateRepository = parcelLockerHibernateRepository;
    }

    @Override
    public ParcelLocker createParcelLocker(String address, Double longitude, Double latitude) {
        ParcelLockerHibernate parcelLockerHibernate = new ParcelLockerHibernate(null, address, longitude, latitude);
        parcelLockerHibernateRepository.save(parcelLockerHibernate);
        return toDomain(parcelLockerHibernate);
    }

    @Override
    public Optional<ParcelLocker> getParcelLocker(Long id) {
        Optional<ParcelLocker> parcelLockerOptional = parcelLockerHibernateRepository.findById(id).map(this::toDomain);
        return parcelLockerOptional;
    }

    @Override
    public List<ParcelLocker> getAllParcelLockers(Pageable pageable) {
        Page<ParcelLockerHibernate> page = parcelLockerHibernateRepository.findAll(pageable);
        List<ParcelLockerHibernate> parcelLockerHibernates = page.getContent();
        return parcelLockerHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeParcelLocker(Long id) {
        parcelLockerHibernateRepository.deleteById(id);
    }

    @Override
    public ParcelLocker updateParcelLocker(Long id, String address, Double longitude, Double latitude) {

        ParcelLockerHibernate parcelLockerHibernate = parcelLockerHibernateRepository.findById(id).get();

        parcelLockerHibernate.setAddress(address);
        parcelLockerHibernate.setLongitude(longitude);
        parcelLockerHibernate.setLatitude(latitude);

        parcelLockerHibernateRepository.save(parcelLockerHibernate);

        return toDomain(parcelLockerHibernate);
    }

    public ParcelLocker toDomain(ParcelLockerHibernate parcelLockerHibernate) {
        return new ParcelLocker(
                parcelLockerHibernate.getId(),
                parcelLockerHibernate.getAddress(),
                parcelLockerHibernate.getLongitude(),
                parcelLockerHibernate.getLatitude());
    }
}
