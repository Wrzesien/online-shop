package com.myproject.shop.infrastructure.adapter;

import com.myproject.shop.domain.model.Basket;
import com.myproject.shop.domain.model.EStatus;
import com.myproject.shop.domain.service.repository.BasketRepository;
import com.myproject.shop.infrastructure.entity.BasketHibernate;
import com.myproject.shop.infrastructure.entity.ClientHibernate;
import com.myproject.shop.infrastructure.entity.ParcelLockerHibernate;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import com.myproject.shop.infrastructure.repository.BasketHibernateRepository;
import com.myproject.shop.infrastructure.repository.ClientHibernateRepository;
import com.myproject.shop.infrastructure.repository.ParcelLockerHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class BasketRepositoryImpl implements BasketRepository {

    private BasketHibernateRepository basketHibernateRepository;
    private ClientHibernateRepository clientHibernateRepository;
    private ParcelLockerHibernateRepository parcelLockerHibernateRepository;

    @Autowired
    public BasketRepositoryImpl(BasketHibernateRepository basketHibernateRepository,
                                ClientHibernateRepository clientHibernateRepository,
                                ParcelLockerHibernateRepository parcelLockerHibernateRepository) {
        this.basketHibernateRepository = basketHibernateRepository;
        this.clientHibernateRepository = clientHibernateRepository;
        this.parcelLockerHibernateRepository = parcelLockerHibernateRepository;
    }

    @Override
    public Basket createBasket(Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<ProductHibernate> products) {

        ClientHibernate clientHibernate = clientHibernateRepository.findById(clientId).get();
        ParcelLockerHibernate parcelLockerHibernate = parcelLockerHibernateRepository.findById(parcelLockerId).get();

        BasketHibernate basketHibernate = new BasketHibernate(
                null,
                clientHibernate,
                parcelLockerHibernate,
                date,
                totalAmount,
                status,
                products);

        basketHibernateRepository.save(basketHibernate);

        return toDomain(basketHibernate);
    }

    @Override
    public Basket addToBasket(Long basketId, Double totalAmount, List<ProductHibernate> products) {

        BasketHibernate basketHibernate = basketHibernateRepository.findById(basketId).get();

        basketHibernate.setTotalAmount(totalAmount);
        basketHibernate.getProducts().addAll(products);

        basketHibernateRepository.save(basketHibernate);

        return toDomain(basketHibernate);
    }

    @Override
    public Optional<Basket> getBasket(Long id) {
        Optional<Basket> basketOptional = basketHibernateRepository.findById(id).map(this::toDomain);
        return basketOptional;
    }

    @Override
    public List<Basket> getAllBaskets(Pageable pageable) {
        Page<BasketHibernate> page = basketHibernateRepository.findAll(pageable);
        List<BasketHibernate> basketHibernates = page.getContent();
        return basketHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeBasket(Long id) {
        basketHibernateRepository.deleteById(id);
    }

    @Override
    public Basket updateBasket(Long id, Long clientId, Long parcelLockerId, LocalDateTime date, Double totalAmount, EStatus status, List<ProductHibernate> products) {

        BasketHibernate basketHibernate = basketHibernateRepository.findById(id).get();
        ClientHibernate clientHibernate = clientHibernateRepository.findById(clientId).get();
        ParcelLockerHibernate parcelLockerHibernate = parcelLockerHibernateRepository.findById(parcelLockerId).get();

        basketHibernate.setClient(clientHibernate);
        basketHibernate.setParcelLocker(parcelLockerHibernate);
        basketHibernate.setDate(date);
        basketHibernate.setTotalAmount(totalAmount);
        basketHibernate.setStatus(status);
        basketHibernate.setProducts(products);

        basketHibernateRepository.save(basketHibernate);

        return toDomain(basketHibernate);
    }

    @Override
    public Basket pay(Long id, Long parcelLockerId) {

        BasketHibernate basketHibernate = basketHibernateRepository.findById(id).get();
        ParcelLockerHibernate parcelLockerHibernate = parcelLockerHibernateRepository.findById(parcelLockerId).get();

        basketHibernate.setParcelLocker(parcelLockerHibernate);
        basketHibernate.setStatus(EStatus.FINISHED);

        basketHibernateRepository.save(basketHibernate);

        return toDomain(basketHibernate);
    }

    public Basket toDomain(BasketHibernate basketHibernate){
        return new Basket(
                basketHibernate.getId(),
                basketHibernate.getClient().getId(),
                basketHibernate.getParcelLocker().getId(),
                basketHibernate.getDate(),
                basketHibernate.getTotalAmount(),
                basketHibernate.getStatus(),
                basketHibernate.getProducts().stream().map(productHibernate -> productHibernate.getId()).collect(Collectors.toList()));
    }
}
