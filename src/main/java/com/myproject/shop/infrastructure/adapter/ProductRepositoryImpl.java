package com.myproject.shop.infrastructure.adapter;

import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.domain.model.Product;
import com.myproject.shop.domain.service.repository.ProductRepository;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import com.myproject.shop.infrastructure.repository.ProductHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private ProductHibernateRepository productHibernateRepository;

    @Autowired
    public ProductRepositoryImpl(ProductHibernateRepository productHibernateRepository) {
        this.productHibernateRepository = productHibernateRepository;
    }

    @Override
    public Product createProduct(String name, ECategory category, Double price, Integer amount) {
        ProductHibernate productHibernate = new ProductHibernate(null, name, category, price, amount);
        productHibernateRepository.save(productHibernate);
        return toDomain(productHibernate);
    }

    @Override
    public Optional<Product> getProduct(Long id) {
        Optional<Product> productOptional = productHibernateRepository.findById(id).map(this::toDomain);
        return productOptional;
    }

    @Override
    public List<Product> getAllProducts(Pageable pageable) {
        Page<ProductHibernate> page = productHibernateRepository.findAll(pageable);
        List<ProductHibernate> productHibernates = page.getContent();
        return productHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeProduct(Long id) {
        productHibernateRepository.deleteById(id);
    }

    @Override
    public Product updateProduct(Long id, String name, ECategory category, Double price, Integer amount) {

        ProductHibernate productHibernate = productHibernateRepository.findById(id).get();

        productHibernate.setName(name);
        productHibernate.setCategory(category);
        productHibernate.setPrice(price);
        productHibernate.setAmount(amount);

        productHibernateRepository.save(productHibernate);

        return toDomain(productHibernate);
    }

    @Override
    public List<Product> findAllByCategory(ECategory category, Pageable pageable) {
        Page<ProductHibernate> page = productHibernateRepository.findAllByCategory(category, pageable);
        List<ProductHibernate> productHibernates = page.getContent();
        return productHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Product> findProductsByFragmentOfName(String nameFragment, Pageable pageable) {
        Page<ProductHibernate> page = productHibernateRepository.findByNameContaining(nameFragment, pageable);
        List<ProductHibernate> productHibernates = page.getContent();
        return productHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public Product toDomain(ProductHibernate productHibernate) {
        return new Product(
                productHibernate.getId(),
                productHibernate.getName(),
                productHibernate.getCategory(),
                productHibernate.getPrice(),
                productHibernate.getAmount());
    }
}
