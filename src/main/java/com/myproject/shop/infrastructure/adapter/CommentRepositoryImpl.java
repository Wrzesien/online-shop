package com.myproject.shop.infrastructure.adapter;

import com.myproject.shop.domain.model.Comment;
import com.myproject.shop.domain.service.repository.CommentRepository;
import com.myproject.shop.infrastructure.entity.ClientHibernate;
import com.myproject.shop.infrastructure.entity.CommentHibernate;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import com.myproject.shop.infrastructure.repository.ClientHibernateRepository;
import com.myproject.shop.infrastructure.repository.CommentHibernateRepository;
import com.myproject.shop.infrastructure.repository.ProductHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private CommentHibernateRepository commentHibernateRepository;
    private ProductHibernateRepository productHibernateRepository;
    private ClientHibernateRepository clientHibernateRepository;

    @Autowired
    public CommentRepositoryImpl(CommentHibernateRepository commentHibernateRepository,
                                 ProductHibernateRepository productHibernateRepository,
                                 ClientHibernateRepository clientHibernateRepository) {
        this.commentHibernateRepository = commentHibernateRepository;
        this.productHibernateRepository = productHibernateRepository;
        this.clientHibernateRepository = clientHibernateRepository;
    }

    @Override
    public Comment createComment(Long productId, Long clientId, String content) {

        ProductHibernate productHibernate = productHibernateRepository.findById(productId).get();
        ClientHibernate clientHibernate = clientHibernateRepository.findById(clientId).get();

        CommentHibernate commentHibernate = new CommentHibernate(
                null,
                productHibernate,
                clientHibernate,
                content);

        commentHibernateRepository.save(commentHibernate);
        return toDomain(commentHibernate);
    }

    @Override
    public Optional<Comment> getComment(Long id) {
        Optional<Comment> commentOptional = commentHibernateRepository.findById(id).map(this::toDomain);
        return commentOptional;
    }

    @Override
    public List<Comment> getAllComments(Pageable pageable) {
        Page<CommentHibernate> page = commentHibernateRepository.findAll(pageable);
        List<CommentHibernate> commentHibernates = page.getContent();
        return commentHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeComment(Long id) {
        commentHibernateRepository.deleteById(id);
    }

    @Override
    public Comment updateComment(Long id, Long productId, Long clientId, String content) {

        CommentHibernate commentHibernate = commentHibernateRepository.findById(id).get();

        commentHibernate.setProduct(productHibernateRepository.findById(productId).get());
        commentHibernate.setClient(clientHibernateRepository.findById(clientId).get());
        commentHibernate.setContent(content);

        commentHibernateRepository.save(commentHibernate);

        return toDomain(commentHibernate);
    }

    @Override
    public List<Comment> findByClientId(Long id, Pageable pageable) {
        Page<CommentHibernate> page = commentHibernateRepository.findByClientId(id, pageable);
        List<CommentHibernate> commentHibernates = page.getContent();
        return commentHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Comment> findByProductId(Long id, Pageable pageable) {
        Page<CommentHibernate> page = commentHibernateRepository.findByProductId(id, pageable);
        List<CommentHibernate> commentHibernates = page.getContent();
        return commentHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    public Comment toDomain(CommentHibernate commentHibernate){

        return new Comment(
                commentHibernate.getId(),
                commentHibernate.getProduct().getId(),
                commentHibernate.getClient().getId(),
                commentHibernate.getContent());
    }
}
