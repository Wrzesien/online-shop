package com.myproject.shop.infrastructure.adapter;

import com.myproject.shop.domain.model.Client;
import com.myproject.shop.domain.service.repository.ClientRepository;
import com.myproject.shop.exception.CommonBadRequestException;
import com.myproject.shop.infrastructure.entity.ClientHibernate;
import com.myproject.shop.infrastructure.repository.ClientHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    private ClientHibernateRepository clientHibernateRepository;

    @Autowired
    public ClientRepositoryImpl(ClientHibernateRepository clientHibernateRepository) {
        this.clientHibernateRepository = clientHibernateRepository;
    }

    @Override
    public Client createClient(String name, String lastName, String email, Double discount, Double accountBalance) {
        ClientHibernate clientHibernate = new ClientHibernate(null, name, lastName, email, discount, accountBalance);
        clientHibernateRepository.save(clientHibernate);
        return toDomain(clientHibernate);
    }

    @Override
    public Optional<Client> getClient(Long id) {
        Optional<Client> clientOptional = clientHibernateRepository.findById(id).map(this::toDomain);
        return clientOptional;
    }

    @Override
    public List<Client> getAllClients(Pageable pageable) {
        Page<ClientHibernate> page = clientHibernateRepository.findAll(pageable);
        List<ClientHibernate> clientHibernates = page.getContent();
        return clientHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeClient(Long id) {
        clientHibernateRepository.deleteById(id);
    }

    @Override
    public Client updateClient(Long id, String name, String lastName, String email, Double discount, Double accountBalance) {

        ClientHibernate clientHibernate = clientHibernateRepository.findById(id).get();

        clientHibernate.setName(name);
        clientHibernate.setLastName(lastName);
        clientHibernate.setEmail(email);
        clientHibernate.setDiscount(discount);
        clientHibernate.setAccountBalance(accountBalance);

        clientHibernateRepository.save(clientHibernate);

        return toDomain(clientHibernate);
    }

    @Override
    public void transferToClientAccount(Long id, Double accountBalance) {

        ClientHibernate clientHibernate = clientHibernateRepository.findById(id).get();

        clientHibernate.setAccountBalance(accountBalance);

        clientHibernateRepository.save(clientHibernate);
    }

    @Override
    public List<Client> findClientsByFragmentOfEmail(String emailFragment, Pageable pageable) {
        Page<ClientHibernate> page = clientHibernateRepository.findByEmailContaining(emailFragment, pageable);
        List<ClientHibernate> clientHibernates = page.getContent();
        return clientHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    public Client toDomain(ClientHibernate clientHibernate) {
        return new Client(
                clientHibernate.getId(),
                clientHibernate.getName(),
                clientHibernate.getLastName(),
                clientHibernate.getEmail(),
                clientHibernate.getDiscount(),
                clientHibernate.getAccountBalance());
    }
}
