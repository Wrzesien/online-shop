package com.myproject.shop.infrastructure.entity;

import javax.persistence.*;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "client")
public class ClientHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "last_name")
    private String lastName;

    private String email;

    private Double discount;

    @Column(name = "account_balance")
    private Double accountBalance;

    @OneToMany(mappedBy = "client", cascade = CascadeType.PERSIST)
    private List<CommentHibernate> comments;

    @OneToMany(mappedBy = "client", cascade = CascadeType.PERSIST)
    private List<BasketHibernate> baskets;

    public ClientHibernate() {
    }

    public ClientHibernate(Long id, String name, String lastName, String email, Double discount, Double accountBalance) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.discount = discount;
        this.accountBalance = accountBalance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public List<CommentHibernate> getComments() {
        return comments;
    }

    public void setComments(List<CommentHibernate> comments) {
        this.comments = comments;
    }

    public List<BasketHibernate> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<BasketHibernate> baskets) {
        this.baskets = baskets;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ClientHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("lastName='" + lastName + "'")
                .add("email='" + email + "'")
                .add("comments=" + comments)
                .add("baskets=" + baskets)
                .toString();
    }
}
