package com.myproject.shop.infrastructure.entity;

import com.myproject.shop.domain.model.ECategory;

import javax.persistence.*;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "product")
public class ProductHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private ECategory category;

    private Double price;

    private Integer amount;

    @ManyToMany(mappedBy = "products")
    private List<BasketHibernate> baskets;

    @OneToMany(mappedBy = "product", cascade = CascadeType.PERSIST)
    private List<CommentHibernate> comments;

    public ProductHibernate() {
    }

    public ProductHibernate(Long id, String name, ECategory category, Double price, Integer amount) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ECategory getCategory() {
        return category;
    }

    public void setCategory(ECategory category) {
        this.category = category;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<BasketHibernate> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<BasketHibernate> baskets) {
        this.baskets = baskets;
    }

    public List<CommentHibernate> getComments() {
        return comments;
    }

    public void setComments(List<CommentHibernate> comments) {
        this.comments = comments;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProductHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("category=" + category)
                .add("price=" + price)
                .add("amount=" + amount)
                .toString();
    }
}
