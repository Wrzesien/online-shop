package com.myproject.shop.infrastructure.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "parcel_locker")
public class ParcelLockerHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String address;

    private Double longitude;

    private Double latitude;

    @OneToMany(mappedBy = "parcelLocker", cascade = CascadeType.PERSIST)
    private List<BasketHibernate> baskets;

    public ParcelLockerHibernate() {
    }

    public ParcelLockerHibernate(Long id, String address, Double longitude, Double latitude) {
        this.id = id;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<BasketHibernate> getBaskets() {
        if (baskets == null){
            return baskets = new ArrayList<>();
        }
        return baskets;
    }

    public void setBaskets(List<BasketHibernate> baskets) {
        this.baskets = baskets;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ParcelLockerHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("address='" + address + "'")
                .add("longitude=" + longitude)
                .add("latitude=" + latitude)
                .toString();
    }
}
