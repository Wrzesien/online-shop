package com.myproject.shop.infrastructure.entity;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "comment")
public class CommentHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductHibernate product;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientHibernate client;

    private String content;

    public CommentHibernate() {
    }

    public CommentHibernate(Long id, ProductHibernate product, ClientHibernate client, String content) {
        this.id = id;
        this.product = product;
        this.client = client;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductHibernate getProduct() {
        return product;
    }

    public void setProduct(ProductHibernate product) {
        this.product = product;
    }

    public ClientHibernate getClient() {
        return client;
    }

    public void setClient(ClientHibernate client) {
        this.client = client;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CommentHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("product=" + product)
                .add("client=" + client)
                .add("content='" + content + "'")
                .toString();
    }
}
