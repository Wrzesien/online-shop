package com.myproject.shop.infrastructure.entity;

import com.myproject.shop.domain.model.EStatus;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "basket")
public class BasketHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientHibernate client;

    @ManyToOne
    @JoinColumn(name = "parcel_locker_id")
    private ParcelLockerHibernate parcelLocker;

    @CreationTimestamp
    private LocalDateTime date;

    @Column(name = "total_amount")
    private Double totalAmount;

    @Enumerated(EnumType.STRING)
    private EStatus status;

    @ManyToMany
    @JoinTable(
            name = "product_basket",
            joinColumns = @JoinColumn(name = "basket_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<ProductHibernate> products;

    public BasketHibernate() {
    }

    public BasketHibernate(Long id, ClientHibernate client, ParcelLockerHibernate parcelLocker, LocalDateTime date, Double totalAmount, EStatus status, List<ProductHibernate> products) {
        this.id = id;
        this.client = client;
        this.parcelLocker = parcelLocker;
        this.date = date;
        this.totalAmount = totalAmount;
        this.status = status;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientHibernate getClient() {
        return client;
    }

    public void setClient(ClientHibernate client) {
        this.client = client;
    }

    public ParcelLockerHibernate getParcelLocker() {
        return parcelLocker;
    }

    public void setParcelLocker(ParcelLockerHibernate parcelLocker) {
        this.parcelLocker = parcelLocker;
    }

    public List<ProductHibernate> getProducts() {
        return products;
    }

    public void setProducts(List<ProductHibernate> products) {
        this.products = products;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(EStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BasketHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("client=" + client)
                .add("parcelLockerId=" + parcelLocker)
                .add("date=" + date)
                .add("totalAmount=" + totalAmount)
                .add("status=" + status)
                .add("products=" + products)
                .toString();
    }
}
