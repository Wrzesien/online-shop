package com.myproject.shop.infrastructure.repository;

import com.myproject.shop.infrastructure.entity.ParcelLockerHibernate;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ParcelLockerHibernateRepository extends PagingAndSortingRepository<ParcelLockerHibernate, Long> {
}
