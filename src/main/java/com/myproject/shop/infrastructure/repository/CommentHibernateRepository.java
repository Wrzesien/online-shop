package com.myproject.shop.infrastructure.repository;

import com.myproject.shop.infrastructure.entity.CommentHibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommentHibernateRepository extends PagingAndSortingRepository<CommentHibernate, Long> {

    Page<CommentHibernate> findByClientId(Long id, Pageable pageable);

    Page<CommentHibernate> findByProductId(Long id, Pageable pageable);
}
