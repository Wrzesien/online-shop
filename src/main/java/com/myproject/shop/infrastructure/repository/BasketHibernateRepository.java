package com.myproject.shop.infrastructure.repository;

import com.myproject.shop.infrastructure.entity.BasketHibernate;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BasketHibernateRepository extends PagingAndSortingRepository<BasketHibernate, Long> {
}
