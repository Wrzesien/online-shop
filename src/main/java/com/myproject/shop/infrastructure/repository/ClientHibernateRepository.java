package com.myproject.shop.infrastructure.repository;

import com.myproject.shop.infrastructure.entity.ClientHibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientHibernateRepository extends PagingAndSortingRepository<ClientHibernate, Long> {

    Page<ClientHibernate> findByEmailContaining(String emailFragment, Pageable pageable);
}
