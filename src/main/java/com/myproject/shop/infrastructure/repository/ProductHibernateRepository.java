package com.myproject.shop.infrastructure.repository;

import com.myproject.shop.domain.model.ECategory;
import com.myproject.shop.infrastructure.entity.ProductHibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductHibernateRepository extends PagingAndSortingRepository<ProductHibernate, Long> {

    Page<ProductHibernate> findAllByCategory(ECategory category, Pageable pageable);

    Page<ProductHibernate> findByNameContaining(String nameFragment, Pageable pageable);
}
