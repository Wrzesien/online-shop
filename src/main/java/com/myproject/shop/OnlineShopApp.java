package com.myproject.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:message.properties")
public class OnlineShopApp {

    public static void main(String[] args) {
        SpringApplication.run(OnlineShopApp.class, args);
    }
}
