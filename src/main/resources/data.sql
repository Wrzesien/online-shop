INSERT INTO product (id, name, category, price, amount) VALUES
(1, 'Samsung S20', 'TELEFONY', 3900, 2),
(2, 'Samsung S20+', 'TELEFONY', 4400, 10),
(3, 'Samsung S10', 'TELEFONY', 3200, 4),
(4, 'Samsung S10+', 'TELEFONY', 3800, 10),
(5, 'Apple iPhone SE 2020 64GB', 'TELEFONY', 2200, 5),
(6, 'Sony LED KD-55XG7077', 'TV', 2200, 10),
(7, 'Samsung QLED QE55Q64R', 'TV', 3500, 12),
(8, 'Panasonic LED TX-58GX810E', 'TV', 2800, 5),
(9, 'Sharp LED 50BL5EA', 'TV', 1500, 18),
(10, 'UGO Mistral 2.0', 'DRONY', 230, 25),
(11, 'Overmax X-bee drone 3.3 Wi-Fi', 'DRONY', 250, 14),
(12, 'Blaupunkt ESC808', 'HULAJNOGI', 1600, 2);

INSERT INTO client (id, name, last_name, email, discount, account_balance) VALUES
(1, 'Anna', 'Kowalska', 'a.kowalska@gmail.com', 0.05, 5000),
(2, 'Tomasz', 'Maruda', 'maruda@gmail.com', 0.02, 3000),
(3, 'Edyta', 'Polak', 'polakedyta@wp.pl', 0.1, 4500),
(4, 'Maciej', 'Nowy', 'm.nowy@poczta.onet.pl', 0.05, 3700),
(5, 'Karolina', 'Zawadzka', 'zawadka18@wp.pl', 0.02, 1200);

INSERT INTO parcel_locker (id, address, longitude, latitude) VALUES
(1, 'CH Arkadia', 52.15, 20.59),
(2, 'Galeria Północna', 52.31, 20.96),
(3, 'Atrium Reduta', 52.12, 20.57),
(4, 'Galeria Młociny', 52.17, 20.55);

INSERT INTO comment (id, product_id, client_id, content) VALUES
(1, 1, 1, 'Super komórka'),
(2, 2, 3, 'Spodziewałem się lepszej jakości'),
(3, 2, 1, 'Trochę za duży wyświetlacz'),
(4, 1, 3, 'Nieadekwatna cena do parametrów. Wolę Apple'),
(5, 8, 2, 'Fajny sklep. Wszystko zgodnie z ustaleniami z mail-a od sklepu'),
(6, 5, 5, 'Przesyłka przyszła z dwudniowym opóźnieniem. Poza tym OK'),
(7, 8, 1, 'Telewizor miał porysowany ekran. Sklep zgodził się wymienić na nowy'),
(8, 4, 2, 'Transakcja przebiegła prawidłowo');


