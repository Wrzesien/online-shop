# **Online Shop**

Application for online shop, using tools technology:

- Spring Boot
- Spring Data
- Hibernate
- Maven
- PostgreSQL
- Git

**API**
=============

**/client**
=============

**/client/add**

- POST : Create a new client

**/client/get/:id**

- GET : Get client

**/client/getAll**

- GET : Get all clients

**/client/delete/:id**

- DELETE : Delete a client

**/client/update**

- PUT : Update a client

**/client/transferAmount/:id**

- PUT : Fund the customer&#39;s account

**/client/getClientsByFragmentOfEmail**

- GET : Get clients whose e-mail address contains a specific phrase


**/product**
=============
**/product/add**

- POST : Create a new product

**/product/get/:id**

- GET : Get product

**/product/getAll**

- GET : Get all products

**/product/delete/:id**

- DELETE : Delete a product

**/product/update**

- PUT : Update a product

**/product/getProductsInCategory**

- GET : Find all products in the selected category

**/product/getProductsByFragmentOfName**

- GET : Find all products by name fragment


**/parcel locker**
=============
**/parcel\_locker/add**

- POST : Create a new parcel locker

**/ parcel\_locker/get/:id**

- GET : Get parcel locker

**/ parcel\_locker/getAll**

- GET : Get all parcel lockers

**/ parcel\_locker/delete/:id**

- DELETE : Delete a parcel locker

**/ parcel\_locker/update**

- PUT : Update a parcel locker


**/comment**
=============
**/comment/add**

- POST : Create a new comment

**/comment/get/:id**

- GET : Get comment

**/comment/getAll**

- GET : Get all comments

**/comment/delete/:id**

- DELETE : Delete a comment

**/comment/update**

- PUT : Update a comment

**/comment/getByClientId/:id**

- GET : Find all comments of the selected client

**/comment/getByProductId/:id**

- GET : Find all comments about the selected product


**/basket**
=============
**/basket/add**

- POST : Create a new basket

**/basket/addToBasket**

- PUT : Add products to the selected basket

**/basket/get/:id**

- GET : Get basket

**/basket/getAll**

- GET : Get all baskets

**/basket/delete/:id**

- DELETE : Delete a basket

**/basket/update**

- PUT : Update a basket

**/basket/pay/:id**

- PUT : Pay for the purchases